
public enum Question {

	Q1("7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getSitzverteilung|1|2|3|4|0|",25),
	Q2("7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getSitzzuteilung|1|2|3|4|0|",10),
	Q3("7|0|5|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getWahlkreisuebersicht|I|1|2|3|4|1|5|34|",25),
	Q4("7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getWahlkreissieger|1|2|3|4|0|",10),
	Q5("7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getUeberhangmandate|1|2|3|4|0|",10),
	Q6("7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getKnappsteSieger|1|2|3|4|0|",20);
	
	
	private final String url;
	private final int percentage;
	
	
	private Question(String url, int percentage){
		this.url = url;
		this.percentage = percentage;
	}
	
	public String getURL(){ 
		return this.url;
	}
	
	public int getPercentage(){
		return this.percentage;
	}
}
