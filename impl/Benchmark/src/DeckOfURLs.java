import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class DeckOfURLs {
	
	private List<Question> urls;
	private int urlP = 0;
	
	public DeckOfURLs(){
//		String[] urls = 
//			{"http://localhost:8080/wahl-client/TestServlet?view=sitzverteilung",
//			 "http://localhost:8080/wahl-client/TestServlet?view=sitzzuteilung",
//			 "http://localhost:8080/wahl-client/TestServlet?view=wahlkreisuebersicht",
//			 "http://localhost:8080/wahl-client/TestServlet?view=wahlkreissieger",
//			 "http://localhost:8080/wahl-client/TestServlet?view=ueberhangmandate",
//			 "http://localhost:8080/wahl-client/TestServlet?view=knappstesieger",
//			 };
		
//		String[] urls = 
//			{"7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getSitzverteilung|1|2|3|4|0|",
//			 "7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getSitzzuteilung|1|2|3|4|0|",
//			 "7|0|5|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getWahlkreisuebersicht|I|1|2|3|4|1|5|34|",
//			 "7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getWahlkreissieger|1|2|3|4|0|",
//			 "7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getUeberhangmandate|1|2|3|4|0|",
//			 "7|0|4|http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/|A3FB590FCBE43D7AFF6D2E0DEF778329|de.tum.wahl.client.ElectionService|getKnappsteSieger|1|2|3|4|0|",
//			 };
		
		
		createDeck();
	}
	
//	public DeckOfURLs(String[] urls, int[] percentages){
//		int s = 0;
//		for(int p: percentages){
//			s += p;
//		}
//		if(urls.length != percentages.length || s != 100)
//			throw new IllegalArgumentException();
//		createDeck(urls, percentages);
//	}
	
	private void createDeck(){
		boolean single = true;
		// Skalierung der Anzahl auf Einer oder Zehner 
		for(Question q: Question.values()){
			int temp = (q.getPercentage()*10)%100;
			if(temp != 0){ 
				single= false;
				break;
			}
		}
		this.urls = new ArrayList<Question>(Question.values().length);
		if(single) {
			for(int i = 0; i < Question.values().length; i++){
				addURL(Question.values()[i], Question.values()[i].getPercentage()/10);
			}
		} else {
			for(int i = 0; i < Question.values().length; i++){
				addURL(Question.values()[i], Question.values()[i].getPercentage());
			}
		}
		
		Collections.shuffle(this.urls);
	}
	
	private void addURL(Question url, int times){
		if(urls != null){
			for(int i = 0; i < times; i++){
				urls.add(url);
			}
		}
			
	}
	
	public Question getURL(){
		if(urlP < urls.size()){
			return urls.get(urlP++);
		} else {
			urlP = 0;
			Collections.shuffle(urls);
			return urls.get(urlP++);
		}
	}
	
	

}
