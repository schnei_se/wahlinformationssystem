import java.io.UnsupportedEncodingException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;



public class Terminal implements Runnable{
	
	private DeckOfURLs deck = new DeckOfURLs();
	final private int tId;
	final private int maxRequests;
	final private double t;
	
	public Terminal(int tId, double t, int maxRequests){
		this.tId = tId;
		this.maxRequests = maxRequests;
		this.t = t;
		
		
	}
	


	@Override
	public void run() {
		
		
		
		
		ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager();
		HttpClient httpclient = new DefaultHttpClient(cm);
		
		PostThread[] threads = new PostThread[maxRequests];
		try {
			
			for(int i = 0; i < maxRequests; i ++){
	HttpPost post = new HttpPost("http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/electionService");
	Question q = deck.getURL(); 
	StringEntity entity = new StringEntity(q.getURL(), 
		    "text/x-gwt-rpc", "UTF-8");
	
	post.setEntity(entity);
	post.addHeader("Connection", "keep-alive");
	post.addHeader("X-GWT-Module-Base", "http://localhost:8080/Wahlinformationssystem/wahlinformationssystem/");
	post.addHeader("X-GWT-Permutation", "B0E808F95C8792FEA212E97374906778");
				threads[i] = new PostThread(httpclient, post, tId, q);
				threads[i].start();
				Thread.sleep((long) ((Math.random()*(1.2-0.8)+0.8)*t*1000));
				
			}
			
		}catch(InterruptedException ie){
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}

}



