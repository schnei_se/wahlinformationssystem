import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;


	 /**
     * A thread that performs a GET.
     */
    class PostThread extends Thread {

        private final HttpClient httpClient;
        private final HttpContext context;
        private final HttpPost httpost;
        private final int tId;
        private final Question q;

        public PostThread(HttpClient httpClient, HttpPost httpost, int tId, Question q) {
            this.httpClient = httpClient;
            this.context = new BasicHttpContext();
            this.httpost = httpost;
            this.tId = tId;
            this.q = q;
        }

        /**
         * Executes the GetMethod and prints some status information.
         */
        @Override
        public void run() {

            //System.out.println(id + " - about to get something from " + httpget.getURI());
            long start = System.currentTimeMillis();
            try {

                // execute the method
                HttpResponse response = httpClient.execute(httpost, context);

                //System.out.println(id + " - get executed");
                // get the response body as an array of bytes
                HttpEntity entity = response.getEntity();
                if (entity != null) {

                    @SuppressWarnings("unused")
		    String res = EntityUtils.toString(entity);
                    long stop = System.currentTimeMillis();
                    long diff = stop - start;
                    System.out.println(tId+"; Q"+q+"; " + diff/1000.0);
                   // System.out.println(id + " - " + bytes.length + " bytes read");
                }

            } catch (Exception e) {
                httpost.abort();
                System.out.println("Terminal ID: "+ tId +" qID: "+ httpost.getURI() + " - error: " + e);
            }
        }

    }