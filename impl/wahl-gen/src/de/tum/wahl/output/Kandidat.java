package de.tum.wahl.output;

import java.text.MessageFormat;

public class Kandidat implements CsvBean {
    private static int nextKandidatenId = 0; 
    private int nr;
    private String titel;
    private String vorname;
    private String name;
    private Integer jahrgang;
    private Partei partei;

    public Kandidat(String titel, String vorname, String name,
	    Integer jahrgang, Partei partei) {
	super();
	this.nr = nextKandidatenId++;
	this.titel = titel;
	this.vorname = vorname;
	this.name = name;
	this.jahrgang = jahrgang;
	this.partei = partei;
    }

    public int getNr() {
	return nr;
    }
    
    public Partei getPartei() {
	return partei;
    }

    public String toCsv() {
	return MessageFormat.format(
		"{0,number,####};\"{1}\";\"{2}\";\"{3}\";{4,number,####};{5}",
		nr, titel, vorname, name, jahrgang,
		partei != null ? partei.getKuerzel() : null);
    }
}
