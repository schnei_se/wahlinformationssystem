package de.tum.wahl.output;

import java.text.MessageFormat;

public class KandidiertIn implements CsvBean {

    private final Kandidat kandidat;
    private final Wahlkreis wahlkreis;

    public KandidiertIn(Kandidat kandidat, Wahlkreis wahlkreis) {
	super();
	this.kandidat = kandidat;
	this.wahlkreis = wahlkreis;
    }

    @Override
    public String toCsv() {
	return MessageFormat.format("{0,number,####};{1,number,####}", kandidat.getNr(),
		wahlkreis.getNr());
    }

}
