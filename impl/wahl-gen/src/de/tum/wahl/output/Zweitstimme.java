package de.tum.wahl.output;

import java.text.MessageFormat;

public class Zweitstimme implements CsvBean {
    private Landesliste landesliste;
    private Wahlkreis wahlkreis;
    private int jahr;

    public Zweitstimme(Landesliste landesliste, Wahlkreis wahlkreis, int jahr) {
	super();
	this.landesliste = landesliste;
	this.wahlkreis = wahlkreis;
	this.jahr = jahr;
    }

    public String toCsv() {
	return MessageFormat.format(
		"{0,number,####};{1,number,####};{2,number,####}",
		landesliste.getNr(), wahlkreis.getNr(), jahr);
    }
}
