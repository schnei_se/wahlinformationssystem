package de.tum.wahl.output;

import java.util.HashMap;
import java.util.Map;

/**
 * Datenbankklasse für Tabelle Parteien.
 * 
 * @author Alexander Ried
 */
public class Partei implements CsvBean {
    public class LandeslisteNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public LandeslisteNotFoundException(Land land, Partei partei) {
	    super("Partei " + partei
		    + " verfügt nicht über Landesliste in Land: " + land);
	}
    }

    public class DirektkandidatNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public DirektkandidatNotFoundException(Wahlkreis wahlkreis,
		Partei partei) {
	    super("Kein Kandidat für WahlkreisNr: " + wahlkreis
		    + " in Partei: " + partei + " gefunden.");
	}

    }

    // private int nr;
    private String kuerzel;
    private String name;

    private transient final Map<Land, Landesliste> landeslisten = new HashMap<Land, Landesliste>();
    private transient final Map<Integer, Kandidat> direktkandidaten = new HashMap<Integer, Kandidat>();

    public Partei(/* int nr, */String kuerzel, String name) {
	super();
	// this.nr = nr;
	this.kuerzel = kuerzel;
	this.name = name;
    }

    public String getKuerzel() {
	return kuerzel;
    }

    @Override
    public String toCsv() {
	StringBuilder sb = new StringBuilder();
	sb.append('"');
	sb.append(kuerzel);
	sb.append("\";\"");
	sb.append(name);
	sb.append('"');
	return sb.toString();
    }

    public void addLandesliste(Landesliste landesliste) {
	landeslisten.put(landesliste.getLand(), landesliste);
    }

    public Landesliste getLandesliste(Land land)
	    throws LandeslisteNotFoundException {
	Landesliste landesliste = landeslisten.get(land);
	if (landesliste == null)
	    throw new LandeslisteNotFoundException(land, this);

	return landesliste;
    }

    public Iterable<Landesliste> getLandeslisten() {
	return landeslisten.values();
    }
    
    public Iterable<Kandidat> getDirektkandidaten() {
	return direktkandidaten.values();
    }

    public Kandidat getDirektkandidat(Wahlkreis wahlkreis)
	    throws DirektkandidatNotFoundException {
	final Kandidat kandidat = direktkandidaten.get(wahlkreis.getNr());
	if (kandidat == null)
	    throw new DirektkandidatNotFoundException(wahlkreis, this);
	return kandidat;
    }

    public void addDirektkandidat(Wahlkreis wahlkreis, Kandidat kandidat) {
	direktkandidaten.put(wahlkreis.getNr(), kandidat);
    }

    @Override
    public String toString() {
	return kuerzel;
    }
}
