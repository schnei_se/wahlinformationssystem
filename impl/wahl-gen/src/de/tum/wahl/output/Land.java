package de.tum.wahl.output;

import java.text.MessageFormat;

/**
 * Model zur Tabelle Laender.
 * 
 * @author Alexander Ried
 */
public class Land implements CsvBean {
    private int nr;
    private String name;

    public Land(int nr, String name) {
	super();
	this.nr = nr;
	this.name = name;
    }

    public int getNr() {
	return nr;
    }
    
    public String getName() {
	return name;
    }
    
    public void setName(String name) {
	this.name = name;
    }
    
    @Override
    public String toCsv() {
	return MessageFormat.format("{0,number,####};\"{1}\"", nr, name);
    }
}
