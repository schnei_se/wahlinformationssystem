package de.tum.wahl.output;

import java.text.MessageFormat;

public class Erststimme implements CsvBean {
    private Kandidat kandidat;
    private Wahlkreis wahlkreis;
    private int jahr;

    public Erststimme(Kandidat kandidat, Wahlkreis wahlkreis, int jahr) {
	super();
	this.kandidat = kandidat;
	this.wahlkreis = wahlkreis;
	this.jahr = jahr;
    }

    public String toCsv() {
	return MessageFormat.format(
		"{0,number,####};{1,number,####};{2,number,####}",
		kandidat.getNr(), wahlkreis.getNr(), jahr);
    }

}
