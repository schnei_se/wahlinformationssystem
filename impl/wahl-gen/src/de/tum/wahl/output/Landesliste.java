package de.tum.wahl.output;

import java.text.MessageFormat;

public class Landesliste implements CsvBean {
    private static int nextNr = 0;
    private int nr;
    private Land land;
    private Partei partei;
    private int jahr;

    public Landesliste(Land land, Partei partei, int jahr) {
	this(nextNr++, land, partei, jahr);
    }

    public Landesliste(int nr, Land land, Partei partei, int jahr) {
	super();
	this.nr = nr;
	this.land = land;
	this.partei = partei;
	this.jahr = jahr;

	partei.addLandesliste(this);
	nextNr = Math.max(nr + 1, nextNr);
    }

    public int getNr() {
	return nr;
    }

    public Land getLand() {
	return land;
    }

    public String toCsv() {
	return MessageFormat.format(
		"{0,number,####};{1,number,####};\"{2}\";{3,number,####}", nr,
		land.getNr(), partei.getKuerzel(), jahr);
    }
}
