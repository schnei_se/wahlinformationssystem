package de.tum.wahl.output;

import java.text.MessageFormat;

public class Wahlkreis implements CsvBean {
    private int nr;
    private String name;
    private Land land;
    private int anzWahlberechtigt;
    private int anzUngueltig1;
    private int anzUngueltig2;
    private int jahr;

    public Wahlkreis(int nr, String name, Land land, int anzWahlberechtigt,
	    int anzUngueltig1, int anzUngueltig2, int jahr) {
	super();
	this.nr = nr;
	this.name = name;
	this.land = land;
	this.anzWahlberechtigt = anzWahlberechtigt;
	this.anzUngueltig1 = anzUngueltig1;
	this.anzUngueltig2 = anzUngueltig2;
	this.jahr = jahr;
    }

    public int getNr() {
	return nr;
    }

    public void setLand(Land land) {
	this.land = land;
    }

    public Land getLand() {
	return land;
    }

    public String toCsv() {
	return MessageFormat
		.format("{0,number,####};\"{1}\";{2,number,####};{3,number,####};{4,number,####};{5,number,####};{6,number,####}",
			nr, name, land.getNr(), anzWahlberechtigt,
			anzUngueltig1, anzUngueltig2, jahr);
    }

    @Override
    public String toString() {
	return nr + " " + name;
    }
}
