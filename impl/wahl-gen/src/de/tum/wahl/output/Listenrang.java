package de.tum.wahl.output;

import java.text.MessageFormat;

public class Listenrang implements CsvBean {
    private Kandidat kandidat;
    private Landesliste landesliste;
    private int rang;
    private int jahr;

    public Listenrang(Kandidat kandidat, Landesliste landesliste, int listenrang, int jahr) {
	super();
	this.kandidat = kandidat;
	this.landesliste = landesliste;
	this.rang = listenrang;
	this.jahr = jahr;
    }

    public String toCsv() {
	return MessageFormat
		.format("{0,number,####};{1,number,####};{2,number,####};{3,number,####}",
			kandidat.getNr(), landesliste.getNr(), rang, jahr);
    }

}
