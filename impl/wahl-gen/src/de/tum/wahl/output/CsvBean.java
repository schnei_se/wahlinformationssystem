package de.tum.wahl.output;

public interface CsvBean {

    String toCsv();

}
