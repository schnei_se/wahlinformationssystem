package de.tum.wahl.input.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import au.com.bytecode.opencsv.CSVReader;

import de.tum.wahl.input.LaenderResource;
import de.tum.wahl.input.ParteienResource;
import de.tum.wahl.output.Land;
import de.tum.wahl.output.Landesliste;
import de.tum.wahl.output.Partei;

public class LandeslistenCsv {
    private Collection<Landesliste> landeslisten = null;
    private int maxLandeslistenNr = 0;
    private CSVReader reader;

    private final LaenderResource laenderResource;
    private final ParteienResource parteienResource;
    private String[] header;

    public LandeslistenCsv(InputStream landeslistenResource,
	    LaenderResource laenderResource, ParteienResource parteienResource)
	    throws IOException {
	if (parteienResource == null)
	    throw new IllegalArgumentException(
		    "ParteienResource darf nicht null sein.");
	this.parteienResource = parteienResource;

	if (laenderResource == null)
	    throw new IllegalArgumentException(
		    "LaenderResource darf nicht null sein.");
	this.laenderResource = laenderResource;

	this.landeslisten = new ArrayList<Landesliste>();

	reader = new CSVReader(new InputStreamReader(landeslistenResource,
		"UTF-8"), ';');

	// dateiinhalt:
	// "Listennummer";"Bundesland";"Partei"
	// 1;"Bayern";"SPD"

	header = reader.readNext();

	parse();
    }

    private void parse() throws NumberFormatException, IOException {
	String[] landeslisteRaw = null;

	while ((landeslisteRaw = reader.readNext()) != null) {
	    // TODO: landeslisten für 2005
	    final Integer jahr = 2009;

	    final int landeslisteNr = Integer.parseInt(landeslisteRaw[0]);
	    final Land land = laenderResource.getLand(landeslisteRaw[1]);
	    final Partei partei = parteienResource.getPartei(landeslisteRaw[2]);
	    addLandesliste(landeslisteNr, land, partei, jahr);
	}
	System.out.println("Landeslisten erfolgreich eingelesen");
    }

    void addLandesliste(int landeslisteNr, Land land, Partei partei, int jahr) {
	final Landesliste landesliste = new Landesliste(landeslisteNr, land,
		partei, jahr);

	landeslisten.add(landesliste);
	maxLandeslistenNr = Math.max(maxLandeslistenNr, landeslisteNr) + 1;
    }

    // public Landesliste getLandesliste(int landNr, String parteiKuerzel, int
    // jahr) {
    // for (Landesliste landesliste : landeslisten) {
    // if (landesliste.getLandNr() == landNr
    // && landesliste.getParteiKuerzel().equals(
    // Generator.normParty(parteiKuerzel))) {
    // return landesliste;
    // }
    // }

    // System.err.println("Keine Landesliste gefunden zu LandNr: " + landNr
    // + ", parteiKuerzel: " + parteiKuerzel);
    // throw new RuntimeException("Keine Landesliste gefunden zu LandNr: "
    // + landNr + ", parteiKuerzel: " + parteiKuerzel);
    //
    // }

    public Iterable<Landesliste> getLandeslisten() {
	return landeslisten;
    }

    String[] getHeader() {
	return header;
    }

}