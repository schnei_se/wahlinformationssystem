package de.tum.wahl.input.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tum.wahl.input.LaenderResource;
import de.tum.wahl.input.ParteienResource;
import de.tum.wahl.input.WahlkreiseResource;
import de.tum.wahl.output.Erststimme;
import de.tum.wahl.output.Land;
import de.tum.wahl.output.Partei;
import de.tum.wahl.output.Wahlkreis;
import de.tum.wahl.output.Zweitstimme;

import au.com.bytecode.opencsv.CSVReader;

public class BtwBundCsv implements LaenderResource, WahlkreiseResource {
    private final CSVReader reader;

    private String[] header;

    private final Map<Integer, Land> laender = new HashMap<Integer, Land>();
    private final Map<Integer, Wahlkreis> wahlkreise = new HashMap<Integer, Wahlkreis>();
    private final List<Stimmergebnis<Erststimme>> ergebnisse1 = new ArrayList<Stimmergebnis<Erststimme>>();
    private final List<Stimmergebnis<Zweitstimme>> ergebnisse2 = new ArrayList<Stimmergebnis<Zweitstimme>>();

    private final ParteienResource parteienResource;

    void addErstimmergebnis(Partei partei, Wahlkreis wahlkreis, int jahr,
	    int anzahl) {
	Stimmergebnis<Erststimme> ergebnis = new Stimmergebnis<Erststimme>(
		Erststimme.class, partei, wahlkreis, jahr, anzahl);
	ergebnisse1.add(ergebnis);
    }

    void addZweitstimmergebnis(Partei partei, Wahlkreis wahlkreis, int jahr,
	    int anzahl) {
	Stimmergebnis<Zweitstimme> ergebnis = new Stimmergebnis<Zweitstimme>(
		Zweitstimme.class, partei, wahlkreis, jahr, anzahl);
	ergebnisse2.add(ergebnis);
    }

    // private Map<String, Land> laenderMapping = null;

    public BtwBundCsv(InputStream resourceStream,
	    ParteienResource parteienResource) throws IOException {
	this.parteienResource = parteienResource;
	this.reader = new CSVReader(new InputStreamReader(resourceStream,
		"ISO-8859-1"), ';');

	for (int i = 0; i < 2; i++)
	    reader.readNext();

	header = reader.readNext();
	reader.readNext();
	reader.readNext();

	parse();
    }

    protected final void parse() throws NumberFormatException, IOException {
	// Nr;Gebiet;gehört;Wahlberechtigte;;;;Wähler;;;;Ungültige;;;;Gültige;;;;SPD;;;;CDU;;;;FDP;;;;DIE
	// LINKE;;;;GRÜNE;;;;CSU;;;;NPD;;;;REP;;;;FAMILIE;;;;Die
	// Tierschutzpartei;;;;PBC;;;;MLPD;;;;BüSo;;;;BP;;;;PSG;;;;Volksabstimmung;;;;ZENTRUM;;;;ADM;;;;CM;;;;DKP;;;;DVU;;;;DIE
	// VIOLETTEN;;;;FWD;;;;ödp;;;;PIRATEN;;;;RRP;;;;RENTNER;;;;Freie
	// Union;;;;Übrige;;;;

	String[] item = null;
	while ((item = reader.readNext()) != null) {
	    if (item[0].length() == 0)
		continue;
	    final Integer nr = Integer.valueOf(item[0]);
	    final String name = item[1];

	    if (item[2].length() == 0) {
		parseBundesgebiet(item);
		break;
	    }

	    final Integer gehoertZu = Integer.parseInt(item[2]);

	    if (gehoertZu == 99) {
		parseLand(nr, name, item);
		continue;
	    }

	    parseWahlkreis(nr, name, gehoertZu, item);
	}

	System.out.println("Wahlkreisergebnisse erfolgreich eingelesen");
    }

    protected void parseWahlkreis(Integer nr, String name, Integer gehoertZu,
	    String[] item) {
	final int anzWahlberechtigt = Integer.parseInt(item[3]);
	final int anzUngueltig1 = Integer.parseInt(item[11]);
	final int anzUngueltig2 = Integer.parseInt(item[13]);

	final Land land = getLand(gehoertZu);
	// wahlkreis objekt anlegen
	Wahlkreis wahlkreis = new Wahlkreis(nr, name, land, anzWahlberechtigt,
		anzUngueltig1, anzUngueltig2, 2009);

	wahlkreise.put(nr, wahlkreis);

	parseWahlkreisErgebnisse(wahlkreis, item);
    }

    private void parseWahlkreisErgebnisse(Wahlkreis wahlkreis, String[] item) {

	// ab spalte 19 beginnen parteiergebnisse
	for (int spalte = 19; spalte < header.length - 4; spalte += 4) {
	    final String parteiKuerzel = header[spalte];
	    final Partei partei = parteienResource.getPartei(parteiKuerzel);

	    if (item[spalte].length() > 0) {
		final int anzStimmen1 = Integer.parseInt(item[spalte]);
		addErstimmergebnis(partei, wahlkreis, 2009, anzStimmen1);
	    }

	    if (item[spalte + 1].length() > 0) {
		final int anzStimmen1 = Integer.parseInt(item[spalte + 1]);
		addErstimmergebnis(partei, wahlkreis, 2005, anzStimmen1);
	    }

	    if (item[spalte + 2].length() > 0) {
		final int anzStimmen2 = Integer.parseInt(item[spalte + 2]);
		addZweitstimmergebnis(partei, wahlkreis, 2009, anzStimmen2);
	    }

	    if (item[spalte + 3].length() > 0) {
		final int anzStimmen2 = Integer.parseInt(item[spalte + 3]);
		addZweitstimmergebnis(partei, wahlkreis, 2005, anzStimmen2);
	    }
	}
    }

    private Land getLand(Integer landNr) {
	Land land = laender.get(landNr);
	if (land == null) {
	    land = createLand(landNr);
	}
	return land;
    }

    private Land createLand(Integer landNr) {
	final Land land = new Land(landNr, null);
	laender.put(landNr, land);
	return land;
    }

    protected void parseBundesgebiet(String[] item) {
	// nichts tun
    }

    protected void parseLand(Integer nr, String name, String[] item) {
	final Land land = getLand(nr);
	land.setName(name);
    }

    @Override
    public Wahlkreis getWahlkreis(Integer wahlkreisNr) {
	final Wahlkreis wahlkreis = wahlkreise.get(wahlkreisNr);
	if (wahlkreis == null)
	    throw new RuntimeException("Kein Wahlkreis für Nr " + wahlkreisNr
		    + " gefunden.");
	return wahlkreis;
    }

    public Iterable<Land> getLaender() {
	return laender.values();
    }

    public Iterable<Wahlkreis> getWahlkreise() {
	return wahlkreise.values();
    }

    String[] getHeader() {
	return header;
    }

    public Land getLand(String landName) {
	for (Land land : getLaender()) {
	    if (landName.equals(land.getName()))
		return land;
	}
	throw new RuntimeException("Kein Land mit Name '" + landName
		+ "' gefunden.");
    }

    public Iterable<Stimmergebnis<Erststimme>> getErststimmergebnisse() {
	return this.ergebnisse1;
    }

    public Iterable<Stimmergebnis<Zweitstimme>> getZweitstimmergebnisse() {
	return this.ergebnisse2;
    }

}
