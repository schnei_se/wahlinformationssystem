package de.tum.wahl.input.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import au.com.bytecode.opencsv.CSVReader;
import de.tum.wahl.input.LaenderResource;
import de.tum.wahl.input.ParteienResource;
import de.tum.wahl.input.WahlkreiseResource;
import de.tum.wahl.output.Kandidat;
import de.tum.wahl.output.KandidiertIn;
import de.tum.wahl.output.Land;
import de.tum.wahl.output.Landesliste;
import de.tum.wahl.output.Listenrang;
import de.tum.wahl.output.Partei;
import de.tum.wahl.output.Partei.LandeslisteNotFoundException;
import de.tum.wahl.output.Wahlkreis;

public class WahlbewerberCsv {

    private CSVReader reader;
    Collection<Kandidat> kandidaten = null;
    Map<Integer, KandidiertIn> kandidiertInMap = null;
    Collection<Listenrang> listenraenge = null;

    private final LaenderResource laenderResource;
    private final ParteienResource parteienResource;
    private final WahlkreiseResource wahlkreiseResource;
    private String[] header;

    public WahlbewerberCsv(InputStream wahlbewerberResource,
	    LaenderResource laenderResource, ParteienResource parteienResource,
	    WahlkreiseResource wahlkreisProvider) throws IOException {
	this.laenderResource = laenderResource;
	this.parteienResource = parteienResource;
	this.wahlkreiseResource = wahlkreisProvider;

	reader = new CSVReader(new InputStreamReader(wahlbewerberResource,
		"UTF-8"), ';');

	// dateiinhalt:
	// "Vorname";"Nachname";"Jahrgang";"Partei";"Wahlkreis";"Bundesland";"Listenplatz";

	header = reader.readNext();

	this.kandidaten = new ArrayList<Kandidat>();
	this.kandidiertInMap = new HashMap<Integer, KandidiertIn>();
	this.listenraenge = new ArrayList<Listenrang>();

	parse();
    }

    private void parse() throws IOException {
	// CSVReader reader05 = new CSVReader(new InputStreamReader(
	// new FileInputStream("./resources/wahlbewerber2005.csv"),
	// "UTF-8"), ';');
	final int jahr = 2009;

	String[] nextLine = null;
	while ((nextLine = reader.readNext()) != null) {
	    // "Vorname";"Nachname";"Jahrgang";"Partei";"Wahlkreis";"Bundesland";"Listenplatz";
	    final String vorname = nextLine[0];
	    final String name = nextLine[1];
	    final Integer jahrgang = Integer.parseInt(nextLine[2]);
	    final String parteiKuerzel = nextLine[3];
	    final String wahlkreisNr = nextLine[4];
	    Wahlkreis wahlkreis = null;
	    if (wahlkreisNr.length() > 0)
		wahlkreis = wahlkreiseResource.getWahlkreis(Integer
			.valueOf(wahlkreisNr));
	    final String landName = nextLine[5];
	    Integer listenplatz = null;
	    if (nextLine[6].length() > 0)
		listenplatz = Integer.valueOf(nextLine[6]);

	    // falls kandidat mitglied in partei ist, zugehöriges partei objekt
	    // suchen
	    Partei partei = null;
	    if (parteiKuerzel.length() > 0)
		partei = parteienResource.getPartei(parteiKuerzel);
	    else
		partei = parteienResource.getPartei("\u00dc" + "brige");

	    // kandidaten anlegen
	    Kandidat kandidat = new Kandidat("", vorname, name, jahrgang,
		    partei);
	    this.kandidaten.add(kandidat);

	    if (wahlkreis != null) {
		// kandidat tritt in wahlkreis an.
		addDirektkandidatur(kandidat, wahlkreis);
	    }

	    if (listenplatz != null) {
		// kandidat steht auf landesliste seiner partei
		if (partei == null)
		    throw new RuntimeException(
			    "Kandidat steht auf Landesliste (Listenplatz: "
				    + listenplatz
				    + ") aber gehört zu keiner Partei (Parteikuerzel: '"
				    + parteiKuerzel + "')");
		final Land land = laenderResource.getLand(landName);
		Landesliste landesliste;
		try {
		    landesliste = partei.getLandesliste(land);
		} catch (LandeslisteNotFoundException e) {
		    throw new RuntimeException(e);
		}
		addListenkandidatur(kandidat, landesliste, listenplatz, jahr);
	    }

	}
	System.out.println("Wahlbewerber erfolgreich eingelesen");
    }

    private void addListenkandidatur(Kandidat kandidat,
	    Landesliste landesliste, Integer listenplatz, int jahr) {
	Listenrang listenrang = new Listenrang(kandidat, landesliste,
		listenplatz, jahr);
	this.listenraenge.add(listenrang);
    }

    private void addDirektkandidatur(Kandidat kandidat, Wahlkreis wahlkreis) {
	KandidiertIn kandidiertIn = new KandidiertIn(kandidat, wahlkreis);
	this.kandidiertInMap.put(kandidat.getNr(), kandidiertIn);
	if (kandidat.getPartei() != null) {
	    kandidat.getPartei().addDirektkandidat(wahlkreis, kandidat);
	}
    }

    public Iterable<Kandidat> getKandidaten() {
	return kandidaten;
    }

    public Iterable<KandidiertIn> getKandidiertIn() {
	return kandidiertInMap.values();
    }

    public Iterable<Listenrang> getListenraenge() {
	return listenraenge;
    }

    String[] getHeader() {
	return header;
    }

    // parse05
    // nextLine = reader05.readNext();
    // while ((nextLine = reader05.readNext()) != null) {
    // final String[] name_ = nextLine[0].split(", ");
    // final String vorname = name_[1];
    // final String name = name_[0];
    // final Integer jahrgang = Integer.parseInt(nextLine[2]);
    // final String parteiKuerzel = normParty(nextLine[7]).trim(); // Partei
    // Kandidat kandidat = new Kandidat(maxKandidatNr, "", vorname, name,
    // jahrgang, parteiKuerzel);
    //
    // int kandnr = maxKandidatNr;
    // if (!kandidaten.containsKey(kandidat)) {
    // this.kandidaten.put(kandidat, kandidat);
    // } else {
    // kandnr = kandidaten.get(kandidat).getNr();
    // }
    //
    // if (nextLine[8].length() > 0) {
    // // Kandidiert in Wahlkreis
    // final int wahlkreisNr = Integer.parseInt(nextLine[8]);
    // this.kandidiertInMap.put(kandnr, wahlkreisNr);
    // }
    // if (nextLine[10].length() > 0) {
    // // Steht auf Landesliste
    // final int listenrang = Integer.parseInt(nextLine[10]);
    //
    // final Land land = landProvider
    // .getLand(convertLand2005(nextLine[9]));
    //
    // final Landesliste landesliste = landeslistenProvider
    // .getLandesliste(land.getNr(), parteiKuerzel, 2005);
    // this.listenraenge.add(new Listenrang(kandnr, landesliste
    // .getNr(), listenrang, 2005));
    // }
    //
    // maxKandidatNr++;
    // }

    // private String convertLand2005(String kuerzel) {
    // if (kuerzel.equals("BB"))
    // return "Brandenburg";
    // else if (kuerzel.equals("BE"))
    // return "Berlin";
    // else if (kuerzel.equals("BW"))
    // return "Baden-Württemberg";
    // else if (kuerzel.equals("BY"))
    // return "Bayern";
    // else if (kuerzel.equals("HB"))
    // return "Bremen";
    // else if (kuerzel.equals("HE"))
    // return "Hessen";
    // else if (kuerzel.equals("HH"))
    // return "Hamburg";
    // else if (kuerzel.equals("MV"))
    // return "Mecklenburg-Vorpommern";
    // else if (kuerzel.equals("NI"))
    // return "Niedersachsen";
    // else if (kuerzel.equals("NW"))
    // return "Nordrhein-Westfalen";
    // else if (kuerzel.equals("RP"))
    // return "Rheinland-Pfalz";
    // else if (kuerzel.equals("SH"))
    // return "Schleswig-Holstein";
    // else if (kuerzel.equals("SL"))
    // return "Saarland";
    // else if (kuerzel.equals("SN"))
    // return "Sachsen";
    // else if (kuerzel.equals("ST"))
    // return "Sachsen-Anhalt";
    // else if (kuerzel.equals("TH"))
    // return "Thüringen";
    // throw new RuntimeException("Ungültiges Länderkürzel: " + kuerzel);
    // }
}
