package de.tum.wahl.input.impl;

import de.tum.wahl.Generator;
import de.tum.wahl.output.CsvBean;
import de.tum.wahl.output.Erststimme;
import de.tum.wahl.output.Kandidat;
import de.tum.wahl.output.Landesliste;
import de.tum.wahl.output.Partei;
import de.tum.wahl.output.Partei.DirektkandidatNotFoundException;
import de.tum.wahl.output.Partei.LandeslisteNotFoundException;
import de.tum.wahl.output.Wahlkreis;
import de.tum.wahl.output.Zweitstimme;

public class Stimmergebnis<T extends CsvBean> implements CsvBean {
    private Class<T> type;
    private Partei partei;
    private final Wahlkreis wahlkreis;
    private final int jahr;
    private final int anzahl;

    public Stimmergebnis(Class<T> type, Partei partei, Wahlkreis wahlkreis,
	    int jahr, int anzahl) {
	super();
	this.type = type;
	this.partei = partei;
	this.wahlkreis = wahlkreis;
	this.jahr = jahr;
	this.anzahl = anzahl;
    }

    public int getAnzahl() {
	return anzahl;
    }

    public Wahlkreis getWahlkreis() {
	return wahlkreis;
    }

    private CsvBean getStimme() {
	if (Erststimme.class.equals(type)) {
	    Kandidat kandidat;
	    try {
		kandidat = partei.getDirektkandidat(wahlkreis);
	    } catch (DirektkandidatNotFoundException e) {
		System.out.println(e.getMessage());
		kandidat = new Kandidat("", "", "Unbekannter Direktkandidat "
			+ partei.getKuerzel(), 2012, partei);
		partei.addDirektkandidat(wahlkreis, kandidat);
		Generator.addUnbekannterKandidat(kandidat);
	    }
	    return new Erststimme(kandidat, wahlkreis, jahr);
	} else if (Zweitstimme.class.equals(type)) {
	    Landesliste landesliste = null;
	    try {
		landesliste = partei.getLandesliste(wahlkreis.getLand());
	    } catch (LandeslisteNotFoundException e) {
		System.out.println(e.getMessage());
		landesliste = new Landesliste(wahlkreis.getLand(), partei, jahr);
		partei.addLandesliste(landesliste);
		Generator.addUnbekannteLandesliste(landesliste);
	    }
	    return new Zweitstimme(landesliste, wahlkreis, jahr);
	} else {
	    throw new IllegalArgumentException("Unbekannter Stimmentyp: "
		    + type.getName());
	}
    }

    @Override
    public String toCsv() {
	return getStimme().toCsv();
    }
}