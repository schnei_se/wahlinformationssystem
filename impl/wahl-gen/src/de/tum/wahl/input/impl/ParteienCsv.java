package de.tum.wahl.input.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;

import de.tum.wahl.input.ParteienResource;
import de.tum.wahl.output.Partei;

public class ParteienCsv implements ParteienResource {
    private CSVReader reader;
    private Map<String, Partei> parteien = null;
    private String[] header;

    // private int nextParteiNr;

    public ParteienCsv(InputStream parteienResource) throws IOException {
	this.parteien = new HashMap<String, Partei>();
	// this.nextParteiNr = 0;

	this.reader = new CSVReader(new InputStreamReader(parteienResource,
		"UTF-8"), ';');

	// dateiinhalt:
	// "Listennummer";"Bundesland";"Partei"
	// 1;"Bayern";"SPD"

	header = reader.readNext();
	parse();
    }

    private void parse() throws NumberFormatException, IOException {
	String[] parteiRaw = null;
	while ((parteiRaw = reader.readNext()) != null) {
	    final String kuerzel = normKuerzel(parteiRaw[0]);
	    final String name = parteiRaw[1];

	    final Partei partei = new Partei(/* nextParteiNr++, */kuerzel, name);
	    parteien.put(kuerzel, partei);
	}
	System.out.println("Parteien erfolgreich eingelesen");
    }

    @Override
    public Partei getPartei(String kuerzel) {
	final Partei partei = parteien.get(normKuerzel(kuerzel));
	if (partei == null)
	    throw new RuntimeException("Keine Partei für Kürzel '" + kuerzel
		    + "' gefunden.");
	return partei;
    }

    @Override
    public Iterable<Partei> getParteien() {
	return parteien.values();
    }

    String[] getHeader() {
	return header;
    }

    String normKuerzel(String kuerzel) {
	kuerzel = kuerzel.toUpperCase();
	if (kuerzel.equals("VIOLETTEN"))
	    return "DIE VIOLETTEN";
	if (kuerzel.equals("TIERSCHUTZ"))
	    return "DIE TIERSCHUTZPARTEI";
	if (kuerzel.equals("VOLKSABST."))
	    return "VOLKSABSTIMMUNG";
	if (kuerzel.startsWith("DIE L"))
	    return "DIE LINKE";
	if (kuerzel.startsWith("HUMANWIRTSCHAFT"))
	    return "HUMANWIRTSCHAFT";
	return kuerzel;
    }

}
