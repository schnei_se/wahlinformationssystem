package de.tum.wahl.input;

import de.tum.wahl.output.Partei;

public interface ParteienResource {

    /**
     * Liefert die Partei zum übergebenen Kürzel. Wirft eine Exception wenn
     * keine Partei gefunden wurde.
     * 
     * @param parteiKuerzel
     * @return
     */
    Partei getPartei(String parteiKuerzel);

    /**
     * Liefert alle Parteien.
     * 
     * @return
     */
    Iterable<Partei> getParteien();

}
