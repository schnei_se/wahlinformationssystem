package de.tum.wahl.input;

import de.tum.wahl.input.impl.Stimmergebnis;
import de.tum.wahl.output.Erststimme;
import de.tum.wahl.output.Wahlkreis;
import de.tum.wahl.output.Zweitstimme;

public interface WahlkreiseResource {

    /**
     * Liefert den Wahlkreis mit der übergebenen Nummer oder wirft eine
     * Exception wenn kein passender Wahlkreis gefunden wurde.
     * 
     * @param wahlkreisNr
     * @return
     */
    Wahlkreis getWahlkreis(Integer wahlkreisNr);

    Iterable<Stimmergebnis<Erststimme>> getErststimmergebnisse();

    Iterable<Stimmergebnis<Zweitstimme>> getZweitstimmergebnisse();

}
