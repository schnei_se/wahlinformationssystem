package de.tum.wahl.input;

import de.tum.wahl.output.Land;

public interface LaenderResource {

    /**
     * Liefert das Land mit dem übergebenen Namen, oder wirft eine Exception,
     * falls kein passendes Land gefunden wurde.
     * 
     * @param landName
     * @return
     */
    Land getLand(String landName);

}
