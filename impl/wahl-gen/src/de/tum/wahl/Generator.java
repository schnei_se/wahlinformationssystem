package de.tum.wahl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import de.tum.wahl.input.impl.BtwBundCsv;
import de.tum.wahl.input.impl.LandeslistenCsv;
import de.tum.wahl.input.impl.Stimmergebnis;
import de.tum.wahl.input.impl.WahlbewerberCsv;
import de.tum.wahl.input.impl.ParteienCsv;
import de.tum.wahl.output.CsvBean;
import de.tum.wahl.output.Kandidat;
import de.tum.wahl.output.Landesliste;

/**
 * 
 * @author Alexander Ried
 * @author Sebastian Schneider
 */
public class Generator {
    public static void main(String[] args) throws IOException {
	Generator generator = new Generator();
	generator.generate();
    }

    private BtwBundCsv btwBundCsv = null;
    private WahlbewerberCsv wahlbewerberCsv = null;
    private ParteienCsv parteienCsv = null;
    private LandeslistenCsv landeslistenCsv = null;
    private static Collection<Kandidat> unbekannteKandidaten = new ArrayList<Kandidat>();
    private static Collection<Landesliste> unbekannteLandeslisten = new ArrayList<Landesliste>();

    private void generate() throws IOException {
	// parteien laden
	final InputStream parteienResource = new FileInputStream(
		"resources/Parteien.csv");
	parteienCsv = new ParteienCsv(parteienResource);

	// wahlkreise (inkl. stimmen 2005 u. 2009) laden
	final InputStream laenderResource = new FileInputStream(
		"resources/BTW_BUND_09.csv");
	btwBundCsv = new BtwBundCsv(laenderResource, parteienCsv);

	// landeslisten laden und partei assoziieren
	final InputStream landeslistenResource = new FileInputStream(
		"resources/landeslisten.csv");
	landeslistenCsv = new LandeslistenCsv(landeslistenResource, btwBundCsv,
		parteienCsv);

	// wahlbewerber laden und parteien und wahlkreise assozieren
	final InputStream wahlbewerberResource = new FileInputStream(
		"resources/wahlbewerber.csv");
	wahlbewerberCsv = new WahlbewerberCsv(wahlbewerberResource, btwBundCsv,
		parteienCsv, btwBundCsv);

	// stimmen ausgeben
	writeStimmen(new FileWriter("target/erststimmen.csv"),
		btwBundCsv.getErststimmergebnisse());
	writeStimmen(new FileWriter("target/zweitstimmen.csv"),
		btwBundCsv.getZweitstimmergebnisse());

	// wahlinformationen ausgeben
	writeIterable(new OutputStreamWriter(new FileOutputStream(
		"target/kandidaten.csv"), "UTF-8"), getKandidaten());
	writeIterable(new OutputStreamWriter(new FileOutputStream(
		"target/kandidiertIn.csv"), "UTF-8"),
		wahlbewerberCsv.getKandidiertIn());
	writeIterable(new OutputStreamWriter(new FileOutputStream(
		"target/laender.csv"), "UTF-8"), btwBundCsv.getLaender());
	writeIterable(new OutputStreamWriter(new FileOutputStream(
		"target/landeslisten.csv"), "UTF-8"), getLandeslisten());
	writeIterable(new OutputStreamWriter(new FileOutputStream(
		"target/listenraenge.csv"), "UTF-8"),
		wahlbewerberCsv.getListenraenge());
	writeIterable(new OutputStreamWriter(new FileOutputStream(
		"target/parteien.csv"), "UTF-8"), parteienCsv.getParteien());
	writeIterable(new OutputStreamWriter(new FileOutputStream(
		"target/wahlkreise.csv"), "UTF-8"), btwBundCsv.getWahlkreise());

    }

    private Iterable<Landesliste> getLandeslisten() {
	final Collection<Landesliste> result = new HashSet<Landesliste>();

	final Iterable<Landesliste> landeslisten = landeslistenCsv
		.getLandeslisten();
	for (Landesliste landesliste : landeslisten)
	    result.add(landesliste);
	for (Landesliste landesliste : unbekannteLandeslisten)
	    result.add(landesliste);

	return result;
    }

    private Iterable<Kandidat> getKandidaten() {
	final Collection<Kandidat> result = new HashSet<Kandidat>();

	final Iterable<Kandidat> kandidaten = wahlbewerberCsv.getKandidaten();
	for (Kandidat kandidat : kandidaten)
	    result.add(kandidat);
	for (Kandidat kandidat : unbekannteKandidaten)
	    result.add(kandidat);

	return result;
    }

    private <T extends CsvBean> void writeStimmen(Writer writer,
	    Iterable<Stimmergebnis<T>> stimmergebnisse) throws IOException {
	try {
	    for (Stimmergebnis<T> stimmergebnis : stimmergebnisse) {
		final Integer anzahl = stimmergebnis.getAnzahl();
		final String csv = stimmergebnis.toCsv();
		if (stimmergebnis.getWahlkreis().getNr() == 219) {
		    for (int i = 0; i < stimmergebnis.getAnzahl(); i++) {
			writer.write(csv);
			writer.write(";1\n");
		    }
		} else {
		    writer.write(csv);
		    writer.write(';');
		    writer.write(anzahl.toString());
		    writer.write('\n');
		}
	    }
	    writer.flush();
	} finally {
	    writer.close();
	}
    }

    private <T extends CsvBean> void writeIterable(Writer writer,
	    Iterable<T> csvIterable) throws IOException {
	final Iterator<T> csvIterator = csvIterable.iterator();
	try {
	    while (csvIterator.hasNext()) {
		writer.write(csvIterator.next().toCsv());
		writer.write('\n');
	    }
	    writer.flush();
	} finally {
	    writer.close();
	}
    }

    public static void addUnbekannterKandidat(Kandidat kandidat) {
	unbekannteKandidaten.add(kandidat);
    }

    public static void addUnbekannteLandesliste(Landesliste landesliste) {
	unbekannteLandeslisten.add(landesliste);
    }
}
