CREATE FUNCTION tan_generator(numberOfTANs integer) RETURNS integer AS $$
DECLARE
	count integer := 0;
BEGIN	
    LOOP
    IF count >= numberOfTANs THEN
        EXIT;  -- exit loop
    END IF;
    INSERT  INTO Tan (Tan)
	VALUES (round(random()*100000));
	count := count + 1; 
    
END LOOP;
RETURN numberOfTANs;
END;
$$ LANGUAGE plpgsql;