SET client_encoding TO 'UTF8';

COPY Laender FROM '${target.dir}/laender.csv' CSV DELIMITER ';' QUOTE '"';

COPY Parteien FROM '${target.dir}/parteien.csv' CSV DELIMITER ';' QUOTE '"';

COPY Landeslisten FROM '${target.dir}/landeslisten.csv' CSV DELIMITER ';' QUOTE '"';

COPY Wahlkreise FROM '${target.dir}/wahlkreise.csv' CSV DELIMITER ';' QUOTE '"';

COPY Kandidaten FROM '${target.dir}/kandidaten.csv' CSV  DELIMITER ';' QUOTE '"' NULL 'null';

COPY KandidiertIn FROM '${target.dir}/kandidiertIn.csv' CSV DELIMITER ';' QUOTE '"';

COPY Listenraenge FROM '${target.dir}/listenraenge.csv' CSV DELIMITER ';' QUOTE '"';

COPY Erststimmen FROM '${target.dir}/erststimmen.csv' CSV DELIMITER ';' QUOTE '"';

COPY Zweitstimmen FROM '${target.dir}/zweitstimmen.csv' CSV DELIMITER ';' QUOTE '"';