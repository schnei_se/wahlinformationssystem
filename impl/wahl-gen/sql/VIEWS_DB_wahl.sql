DROP VIEW IF EXISTS AnzahlZweitstimmenProLand, ZweitstimmenBundesweit, 
  ZweitstimmenBundesweitRelativ, ZweitstimmenRelativProLand, 
  ErststimmenProKandidat, AnzahlMaxErststimmenProWahlkreis, Direktmandate, 
  AnzahlDirektmandateProLand, BerechtigteParteien, Divisoren, 
  LagueOberverteilung, AnzahlMandateDurchZweitstimmen,
  AnzahlMandateDurchZweitstimmenProLandObergrenze, LagueUnterverteilung,
  AnzahlMandateDurchZweitstimmenProLand, AnzahlUeberhangMandateProLand,
  Sitzverteilung, KnappsteSieger, KnappsteVerlierer, Ueberhangmandate,
  Sitzzuteilung, Wahlkreissieger, Wahlkreisuebersicht
  CASCADE;

-- Zweitstimmen pro Bundesland und Partei
CREATE VIEW AnzahlZweitstimmenProLand(parteiKuerzel, landNr, anzahl) AS
    SELECT l.parteiKuerzel, l.landNr, SUM(z.anzahl) 
    FROM Landeslisten l, Zweitstimmen z 
    WHERE l.nr = z.landeslistenr
      AND z.jahr = 2009
      AND l.jahr = 2009
    GROUP BY l.parteiKuerzel, l.landNr;
    
-- Zweitstimmen bundesweit pro Partei 
CREATE VIEW ZweitstimmenBundesweit(parteiKuerzel, anzahl) AS 
    SELECT parteiKuerzel, SUM(anzahl)
    FROM AnzahlZweitstimmenProLand
    GROUP BY parteiKuerzel;
    
-- Zweitstimmen bundesweit relativ pro Partei
CREATE VIEW ZweitstimmenBundesweitRelativ(parteiKuerzel, prozent) AS
    SELECT parteiKuerzel, 100. * anzahl / (SELECT SUM(anzahl) FROM Zweitstimmen WHERE jahr = 2009)
    FROM ZweitstimmenBundesweit;
 
-- Prozentuale Anzahl der Stimmen für eine Partei pro Land
CREATE VIEW ZweitstimmenRelativProLand(parteiKuerzel, landNr, quote) AS
    SELECT l.parteiKuerzel, l.landNr, l.anzahl / b.anzahl 
    FROM AnzahlZweitstimmenProLand l, ZweitstimmenBundesweit b
    WHERE l.parteiKuerzel = b.parteiKuerzel;

-- Anzahl Erststimmen pro Kandidat und Wahlkreis
CREATE VIEW ErststimmenProKandidat(kandidatNr, wahlkreisNr, anzahl) AS
    SELECT kandidatNr, wahlkreisNr, sum(anzahl)
    FROM Erststimmen
    WHERE jahr = 2009
    GROUP BY kandidatNr, wahlkreisNr;
    
CREATE VIEW AnzahlMaxErststimmenProWahlkreis(wahlkreisNr, anzahl) AS
    SELECT wahlkreisNr, MAX(anzahl) 
    FROM ErststimmenProKandidat
    GROUP BY wahlkreisNr;

-- Direktmandate pro Wahlkreis
CREATE VIEW Direktmandate(wahlkreisNr, kandidatNr, anzahl) AS 
    SELECT e.wahlkreisNr, e.kandidatNr, e.anzahl
    FROM ErststimmenProKandidat e, AnzahlMaxErststimmenProWahlkreis f
    WHERE e.anzahl = f.anzahl
      AND e.wahlkreisNr = f.wahlkreisNr;

-- Anzahl Direktmandate pro Land
CREATE VIEW AnzahlDirektmandateProLand(landNr, parteiKuerzel, anzahl) AS 
    SELECT w.landNr, k.parteiKuerzel, COUNT(*) 
    FROM Wahlkreise w, Direktmandate d, Kandidaten k
    WHERE w.nr = d.wahlkreisNr
      AND d.kandidatNr = k.nr
    GROUP BY w.landNr, k.parteiKuerzel;

-- Kuerzel der Parteien berechnen, die alle Sperrklauseln erfüllen
CREATE VIEW BerechtigteParteien(parteiKuerzel) AS 
    -- Mindestens drei Direktkandidaten
    SELECT k.parteiKuerzel
    FROM Kandidaten k, Direktmandate d
    WHERE k.nr = d.kandidatNr
    GROUP BY k.parteiKuerzel
    HAVING COUNT(*) >= 3
    UNION
    -- oder mindestens 5% bundesweit
    SELECT parteiKuerzel
    FROM ZweitstimmenBundesweitRelativ
    WHERE prozent >= 5.0;
    
-- Divisoren für Sainte-Lague-Verfahren
CREATE VIEW Divisoren(divisor) AS
    SELECT * 
    FROM generate_series(1,600,2);
    
-- Oberverteilung nach Sainte-Lague-Verfahren
CREATE VIEW LagueOberverteilung(parteiKuerzel, divisor, quotient) AS
    SELECT pas.ParteiKuerzel, divisor, pas.anzahl/d.divisor as quotient
    FROM ZweitstimmenBundesweit pas, Divisoren d, BerechtigteParteien p
    WHERE p.parteiKuerzel = pas.parteiKuerzel
    ORDER BY quotient DESC
    LIMIT 598;
    
-- Anzahl der Sitze durch Zweitstimmen
CREATE VIEW AnzahlMandateDurchZweitstimmen(parteiKuerzel, anzahl) AS
    SELECT parteiKuerzel, COUNT(*)
    FROM LagueOberverteilung
    GROUP BY parteiKuerzel;

-- Performance: Obergrenze für Divisor als Anzahl der Sitze nach 
--   Quotientenverfahren (eine Iteration, Fehlertolerant) berechnen
CREATE VIEW AnzahlMandateDurchZweitstimmenProLandObergrenze(landNr, parteiKuerzel, divisorMax) AS
    SELECT m.landNr, m.parteiKuerzel, 4*m.quote*n.anzahl -- *2 (divisoren nur ungerade), *2 zur Sicherheit 
    FROM ZweitstimmenRelativProLand m, AnzahlMandateDurchZweitstimmen n
    WHERE m.parteiKuerzel = n.parteiKuerzel;
    
-- Unterverteilung nach Sainte-Lague-Verfahren
CREATE VIEW LagueUnterverteilung(landNr, parteiKuerzel, rank) AS
    SELECT z.landNr, z.parteiKuerzel, RANK() OVER (PARTITION BY z.parteiKuerzel ORDER BY z.anzahl/d.divisor DESC)
    FROM Divisoren d, AnzahlZweitstimmenProLand z, AnzahlMandateDurchZweitstimmenProLandObergrenze dm
    WHERE d.divisor < dm.divisorMax
      AND z.landNr = dm.landNr
      AND z.parteiKuerzel = dm.parteiKuerzel;
      
CREATE VIEW AnzahlMandateDurchZweitstimmenProLand(landNr, parteiKuerzel, anzahl) AS
    SELECT m.landNr, m.parteiKuerzel, COUNT(*) 
    FROM LagueUnterverteilung m, AnzahlMandateDurchZweitstimmen n
    WHERE m.rank <= n.anzahl
      AND m.parteiKuerzel = n.parteiKuerzel
    GROUP BY m.landNr, m.parteiKuerzel;
  
-- Anzahl der Überhangmandate pro Partei
CREATE VIEW AnzahlUeberhangMandateProLand(parteiKuerzel, landNr, anzahl) AS
    SELECT m.parteiKuerzel,  m.landNr, SUM(GREATEST(0, d.anzahl -  m.anzahl))
    FROM AnzahlMandateDurchZweitstimmenProLand m, AnzahlDirektmandateProLand d
    WHERE m.parteiKuerzel = d.parteiKuerzel
      AND m.landNr = d.landNr
    GROUP BY m.parteiKuerzel, m.landNr;

-- Endgültige Anzahl der Sitze pro Partei
CREATE VIEW Sitzverteilung(parteiKuerzel, anzahl) AS 
    SELECT t.parteiKuerzel, SUM(t.anzahlSitze) 
    FROM (
	    SELECT parteiKuerzel, anzahl
	    FROM AnzahlUeberhangMandateProLand 
	    UNION ALL 
	    SELECT *
	    FROM AnzahlMandateDurchZweitstimmen
    ) AS t(parteiKuerzel, anzahlSitze)
    GROUP BY parteiKuerzel;

-- Knappsten Sieger
CREATE VIEW KnappsteSieger(id, wahlkreisNr, Vorname, Name, parteikuerzel, vorsprung) AS
WITH
	AnzahlZweiterErststimmenProWahlkreis(wahlkreisNr, anzahl) AS (
	SELECT e.wahlkreisNr, MAX(e.anzahl)
	FROM ErststimmenProKandidat e, Direktmandate d
	WHERE e.kandidatNr not in  (select kandidatNr from Direktmandate)
 	GROUP BY e.wahlkreisNr),

	Zweiter(wahlkreisNr, kandidat, anzahl) AS (
	SELECT e.wahlkreisNR, e.KandidatNR, e.anzahl
	FROM ErststimmenProKandidat e, AnzahlZweiterErststimmenProWahlkreis f
    WHERE e.anzahl = f.anzahl
      AND e.wahlkreisNr = f.wahlkreisNr),
      
    Vorsprung(kandidatNr, stimmen) AS (
	SELECT d.kandidatNR, d.anzahl-z.anzahl
	FROM Direktmandate d, Zweiter z
	WHERE d.wahlkreisNr=z.wahlkreisNr)
    
	SELECT round(random()*1000000), wahlkreisnr, vorname, name, parteikuerzel, stimmen
	FROM (SELECT ki.wahlkreisNR, k.vorname, k.name,  k.parteiKuerzel, v.stimmen, rank() OVER (PARTITION BY Parteikuerzel ORDER BY v.stimmen) as Rang
		FROM Vorsprung v, Kandidaten k, Kandidiertin ki
    	WHERE k.nr = v.kandidatnr AND ki.kandidatnr = k.nr) temp
    WHERE temp.Rang <= 10;
	
     
		
	
-- Knappste Verlierer      
CREATE VIEW KnappsteVerlierer(id, wahlkreisnr, vorname, name, parteikuerzel, differenz ) AS	
WITH 
  VerliererKandidaten(KandidatNr, wahlkreisNr) AS ( 
  SELECT k.nr, ki.wahlkreisNr, k.parteiKuerzel
  FROM Kandidaten k, KandidiertIn ki
  WHERE ki.kandidatnr = k.nr AND k.nr not in (select d.kandidatnr
  								FROM  Direktmandate d)
  								
  ),
  
  DifferenzAufDirektmandat(wahlkreisnr, kandidatnr, parteiKuerzel, anzahl) AS (
  	SELECT v.wahlkreisnr, v.kandidatnr, v.parteiKuerzel, d.anzahl-epk.anzahl
  	FROM Direktmandate d, ErststimmenProKandidat epk, VerliererKandidaten v
  	WHERE v.kandidatnr = epk.kandidatnr AND v.wahlkreisNr = d.wahlkreisNr 
  )
  
  SELECT round(random()*1000000), wahlkreisnr, vorname, name, parteikuerzel,anzahl  
  FROM (SELECT d.wahlkreisnr, k.vorname, k.name, k.parteiKuerzel, d.anzahl,rank() OVER (PARTITION BY k.Parteikuerzel ORDER BY d.anzahl) as Rang
  		FROM DifferenzAufDirektmandat d, Kandidaten k
  		WHERE d.kandidatnr = k.nr ) temp
  WHERE temp.Rang <= 10;
  
  
      
-- Ueberhangmandate
CREATE VIEW Ueberhangmandate(parteiKuerzel, landName, anzahl) AS    
    SELECT u.parteiKuerzel, l.name, u.anzahl
    FROM AnzahlUeberhangMandateProLand u, Laender l
    WHERE l.nr = u.landNr
      AND u.anzahl > 0;

-- Listenraenge ohne direkt gewählte Kandidaten
CREATE VIEW ListenraengeClean(landeslisteNr, kandidatNr, rang) AS
    SELECT r.landeslisteNr, r.kandidatNr, RANK() OVER (PARTITION BY r.landeslisteNr ORDER BY r.rang)
    FROM Listenraenge r
    WHERE r.jahr = 2009 
      AND NOT EXISTS (
        SELECT * FROM Direktmandate d WHERE d.kandidatNr = r.kandidatNr
      );
      
-- Listenmandate
CREATE VIEW Listenmandate(landeslisteNr, kandidatNr) AS
    SELECT r.landeslisteNr, r.kandidatNr
    FROM AnzahlMandateDurchZweitstimmenProLand m 
    LEFT JOIN AnzahlDirektmandateProLand d ON (d.landNr = m.landNr AND d.parteiKuerzel = m.parteiKuerzel)
    LEFT JOIN AnzahlUeberhangMandateProLand u ON (u.landNr = m.landNr AND u.parteiKuerzel = m.parteiKuerzel),
      Landeslisten l, ListenraengeClean r
    WHERE m.parteiKuerzel = l.parteiKuerzel
      AND m.landNr = l.landNr
      AND r.landeslisteNr = l.nr
      AND r.rang <= m.anzahl - COALESCE(d.anzahl,0) + COALESCE(u.anzahl,0);
      
CREATE VIEW Sitzzuteilung(kandidatNr, vorname, name, jahrgang, parteiKuerzel) AS
WITH Sitzzuteilung(kandidatNr) AS (
    SELECT kandidatNr 
    FROM Direktmandate
    UNION ALL
    SELECT kandidatNr
    FROM Listenmandate)
    
    SELECT kandidatNr, vorname, name, jahrgang, parteiKuerzel 
    FROM Sitzzuteilung s, Kandidaten k
    WHERE s.kandidatNr = k.nr;
      
CREATE VIEW Wahlkreissieger(wahlkreisName, kandidatName, parteiKuerzel) AS
    SELECT w.name, k.name, l.parteiKuerzel
    FROM Wahlkreise w, Direktmandate d, Kandidaten k, Landeslisten l, Zweitstimmen z
    WHERE w.nr = d.wahlkreisNr
      AND d.kandidatNr = k.nr
      AND l.nr = z.landeslisteNr
      AND w.nr = z.wahlkreisNr
      AND z.anzahl = (SELECT MAX(anzahl) FROM Zweitstimmen e WHERE z.wahlkreisNr = e.wahlkreisNr);
      
CREATE VIEW Wahlkreisuebersicht(wahlkreisNr, wahlbeteiligung, direktkandidatNr, parteiKuerzel, anzahl, anzahlRel) AS
WITH AnzahlZweitstimmenStimmenProWahlkreis(wahlkreisNr, anzahl) AS (
    SELECT wahlkreisNr, SUM(anzahl) 
    FROM Zweitstimmen
    GROUP BY wahlkreisNr)
    
    SELECT w.nr, 100 * sz.anzahl/w.anzwahlberechtigte, d.kandidatNr, l.parteiKuerzel, z.anzahl, 100 * z.anzahl/sz.anzahl
    FROM Wahlkreise w, Direktmandate d, Zweitstimmen z, Landeslisten l, AnzahlZweitstimmenStimmenProWahlkreis sz
    WHERE w.nr = d.wahlkreisNr
      AND d.wahlkreisNr = w.nr
      AND z.landeslisteNr = l.nr
      AND z.wahlkreisNr = w.nr
      AND sz.wahlkreisNr = w.nr;

      
      