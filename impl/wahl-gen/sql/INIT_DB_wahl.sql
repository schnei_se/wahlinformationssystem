-- TODO: attribut Listenraenge.Jahr entfernen. LandelisteNr ist bereits Jahresabhängig
	
CREATE TABLE Laender (
	Nr INT PRIMARY KEY,
	Name VARCHAR(25) NOT NULL
);

CREATE TABLE Parteien (
	Kuerzel CHAR(40) PRIMARY KEY,
	Name VARCHAR(255) NOT NULL
);

CREATE TABLE Landeslisten (
	Nr INT PRIMARY KEY,
	LandNr INT NOT NULL REFERENCES Laender,
	ParteiKuerzel CHAR(40) NOT NULL REFERENCES Parteien,
	Jahr INT NOT NULL
);

CREATE TABLE Wahlkreise (
	Nr INT PRIMARY KEY,
	Name VARCHAR(255) NOT NULL,
	LandNr INT NOT NULL REFERENCES Laender,
	AnzWahlberechtigte INT NOT NULL,
	AnzUngueltig1 INT NOT NULL,
	AnzUngueltig2 INT NOT NULL,
	Jahr INT NOT NULL
);

CREATE TABLE Kandidaten (
	Nr INT PRIMARY KEY,
	Titel VARCHAR(10),
	Vorname VARCHAR(100) NOT NULL,
	Name VARCHAR(100) NOT NULL,
	Jahrgang INT,
	ParteiKuerzel CHAR(40) REFERENCES Parteien
);

CREATE TABLE kandidiertIn (
	KandidatNr INT PRIMARY KEY REFERENCES Kandidaten,
	WahlkreisNr INT NOT NULL REFERENCES Wahlkreise
);

CREATE TABLE Listenraenge (
	KandidatNr INT REFERENCES Kandidaten,
	LandeslisteNr INT REFERENCES Landeslisten,
	Rang INT NOT NULL,
	Jahr INT,
	PRIMARY KEY (KandidatNr, LandeslisteNr, Jahr)
);

CREATE TABLE Erststimmen (
	KandidatNr INT NOT NULL REFERENCES Kandidaten,
	WahlkreisNr INT NOT NULL REFERENCES Wahlkreise,
	Jahr INT NOT NULL,
	anzahl int not null
);

CREATE TABLE Zweitstimmen (
	LandeslisteNr INT NOT NULL REFERENCES Landeslisten,
	WahlkreisNr INT NOT NULL REFERENCES Wahlkreise,
	Jahr INT NOT NULL,
	anzahl int not null
);

CREATE TABLE Tan (
	Tan INT PRIMARY KEY
);