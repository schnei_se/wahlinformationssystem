DROP TABLE IF EXISTS 
	Laender, 
	Parteien, 
	Landeslisten, 
	Wahlkreise, 
	Kandidaten, 
	kandidiertIn, 
	Listenraenge, 
	Erststimmen, 
	Zweitstimmen, 
	Tan
	CASCADE;

DROP VIEW IF EXISTS
	fuenfprozenthuerde,
	parteianzstimmen,
	parteiprozent,
	direktkanidaten,
	kandidatenanzstimmen, 
 	direktkandidatenoderfuenfprozenthuerde,
	mindreiDirektkandidaten CASCADE;