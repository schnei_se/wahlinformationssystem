package de.tum.wahl.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.tum.wahl.shared.GesamteGewinnerVerlierer;
import de.tum.wahl.shared.GesamteWahlkreisuebersicht;
import de.tum.wahl.shared.Sitzverteilung;
import de.tum.wahl.shared.Sitzzuteilung;
import de.tum.wahl.shared.Ueberhangmandat;
import de.tum.wahl.shared.Wahlkreissieger;
import de.tum.wahl.shared.Wahlkreisuebersicht;
import de.tum.wahl.shared.WahlkreisuebersichtErststimme;

@RemoteServiceRelativePath("electionService")
public interface ElectionService extends RemoteService {
    List<Sitzverteilung> getSitzverteilung();

    List<Sitzzuteilung> getSitzzuteilung();

    GesamteWahlkreisuebersicht getWahlkreisuebersicht(int wahlkreisnr);
    
    List<Wahlkreisuebersicht> getWahlkreisuebersichtEinzelstimmen(int wahlkreisnr);

    List<Wahlkreissieger> getWahlkreissieger();

    List<Ueberhangmandat> getUeberhangmandate();

    GesamteGewinnerVerlierer getKnappsteGewinnerVerlierer();
    List<WahlkreisuebersichtErststimme> getWahlkreisuebersichtErststimme(int wahlkreisnr);
    
    Void reset();
}
