package de.tum.wahl.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.tum.wahl.shared.Stimmzettel;
import de.tum.wahl.shared.Vote;
import de.tum.wahl.shared.VoteException;

@RemoteServiceRelativePath("voteService")
public interface VoteService extends RemoteService {
	
	Stimmzettel getStimmzettel() throws VoteException;
	public void vote(Vote stimmzettel) throws VoteException;
	
}
