package de.tum.wahl.client.presenter;

import java.util.List;


import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import de.tum.wahl.shared.GesamteWahlkreisuebersicht;
import de.tum.wahl.shared.WahlkreisuebersichtErststimme;
import de.tum.wahl.client.ElectionServiceAsync;
import de.tum.wahl.client.parser.ParsingCallback;
import de.tum.wahl.client.parser.WahlkreisuebersichtErstimmeParser;
import de.tum.wahl.client.parser.WahlkreisuebersichtParser;
public class WahlkreisuebersichtPresenter {
	
	private ElectionServiceAsync electionService;
	private Display display;
	private boolean zweitstimmen;
	public interface Display {
		HasSelectionHandlers<SuggestOracle.Suggestion> getSugestBox();
		FlowPanel getDataPanel();
	}
	
	
	public WahlkreisuebersichtPresenter(ElectionServiceAsync electionService, boolean zweitstimmen, Display display){
		this.electionService = electionService;
		this.display = display;
		this.zweitstimmen = zweitstimmen;
		bind();
	}
	
	private void bind(){
		display.getSugestBox().addSelectionHandler(new SelectionHandler<SuggestOracle.Suggestion>(){

			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				doLoadAndShowWahlkreisuebersicht(event.getSelectedItem().getReplacementString());
			}
			
		});
	}

	private void doLoadAndShowWahlkreisuebersicht(String replacementString) {
		String[] splitted = replacementString.split(" ");
		try {
			int wahlkreisnr = Integer.parseInt(splitted[0]);
			if(zweitstimmen) {
			electionService
		    .getWahlkreisuebersicht(wahlkreisnr, new ParsingCallback<GesamteWahlkreisuebersicht>(
			    display.getDataPanel(),
			    new WahlkreisuebersichtParser()));
			} else {
				electionService
			    .getWahlkreisuebersichtErststimme(wahlkreisnr, new ParsingCallback<List<WahlkreisuebersichtErststimme>>(
				    display.getDataPanel(),
				    new WahlkreisuebersichtErstimmeParser()));
			}
		} catch (Exception e){
			Window.alert("Fehlerhafte Eingabe");
		}
	}
	
}
