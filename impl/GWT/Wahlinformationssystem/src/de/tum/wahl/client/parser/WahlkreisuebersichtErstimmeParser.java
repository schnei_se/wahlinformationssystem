package de.tum.wahl.client.parser;

import java.util.List;

import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.Table;

import de.tum.wahl.client.Wahlinformationssystem;
import de.tum.wahl.shared.WahlkreisuebersichtErststimme;

public class WahlkreisuebersichtErstimmeParser implements ResultParser<List<WahlkreisuebersichtErststimme>>{

	@Override
	public void parse(List<WahlkreisuebersichtErststimme> e, ComplexPanel p) {
		int rowIndex = 0;
		DataTable dt = DataTable.create();
		dt.addColumn(ColumnType.STRING, "Partei");
		dt.addColumn(ColumnType.NUMBER, "Jahr");
		dt.addColumn(ColumnType.NUMBER, "Stimmen [%]");
		dt.addColumn(ColumnType.NUMBER, "Stimmen [absolut]");
		dt.addRows(e.size());
		for (WahlkreisuebersichtErststimme w : e) {
		    dt.setValue(rowIndex, 0, w.getParteikuerzel());
		    dt.setValue(rowIndex, 1, w.getJahr());
		    dt.setValue(rowIndex, 2, w.getRelativ());
		    dt.setValue(rowIndex, 3, w.getAbsolut());
		    rowIndex++;
		}
		Wahlinformationssystem.createFractionOptions().format(dt, 2);
		p.add(new Label("Übersicht der Erststimmen (Parteien der Kandidaten) für Wahlkreis "+e.get(0).getWahlkreisnr()));
		p.add(new Table(dt, Wahlinformationssystem.createTableOptions()));
	}

}
