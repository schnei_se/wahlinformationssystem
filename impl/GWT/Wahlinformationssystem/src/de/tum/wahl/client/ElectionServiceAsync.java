package de.tum.wahl.client;

import java.util.List;
import com.google.gwt.user.client.rpc.AsyncCallback;

import de.tum.wahl.shared.GesamteGewinnerVerlierer;
import de.tum.wahl.shared.GesamteWahlkreisuebersicht;
import de.tum.wahl.shared.Sitzverteilung;
import de.tum.wahl.shared.Sitzzuteilung;
import de.tum.wahl.shared.Ueberhangmandat;
import de.tum.wahl.shared.Wahlkreissieger;
import de.tum.wahl.shared.Wahlkreisuebersicht;
import de.tum.wahl.shared.WahlkreisuebersichtErststimme;

public interface ElectionServiceAsync {

    void getSitzverteilung(AsyncCallback<List<Sitzverteilung>> callback);

    void getSitzzuteilung(AsyncCallback<List<Sitzzuteilung>> callback);

    void getWahlkreisuebersicht(int wahlkreisnr,
	    AsyncCallback<GesamteWahlkreisuebersicht> callback);

    void getWahlkreissieger(AsyncCallback<List<Wahlkreissieger>> callback);

    void getUeberhangmandate(AsyncCallback<List<Ueberhangmandat>> callback);


	void getWahlkreisuebersichtEinzelstimmen(int wahlkreisnr,
			AsyncCallback<List<Wahlkreisuebersicht>> callback);

	void getWahlkreisuebersichtErststimme(int wahlkreisnr,
			AsyncCallback<List<WahlkreisuebersichtErststimme>> callback);

	void reset(AsyncCallback<Void> callback);

	void getKnappsteGewinnerVerlierer(
			AsyncCallback<GesamteGewinnerVerlierer> callback);

}
