package de.tum.wahl.client.parser;

import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.Table;

import de.tum.wahl.client.Wahlinformationssystem;
import de.tum.wahl.shared.GesamteGewinnerVerlierer;
import de.tum.wahl.shared.KnappsteVerlierer;
import de.tum.wahl.shared.Knappstesieger;

public class KnappstesiegerParser implements ResultParser<GesamteGewinnerVerlierer> {

    @Override
    public void parse(GesamteGewinnerVerlierer e, ComplexPanel p) {
	DataTable dt = DataTable.create();
	dt.addColumn(ColumnType.NUMBER, "Wahlkreisnr");
	dt.addColumn(ColumnType.STRING, "Vorname");
	dt.addColumn(ColumnType.STRING, "Name");
	dt.addColumn(ColumnType.STRING, "Parteikürzel");
	dt.addColumn(ColumnType.NUMBER, "Vorsprung");
	dt.addRows(e.getSieger().size());
	int rowIndex = 0;
	for (Knappstesieger k : e.getSieger()) {
	    dt.setValue(rowIndex, 0, k.getWahlkreisnr());
	    dt.setValue(rowIndex, 1, k.getVorname());
	    dt.setValue(rowIndex, 2, k.getName());
	    dt.setValue(rowIndex, 3, k.getParteikuerzel());
	    dt.setValue(rowIndex, 4, k.getVorsprung());
	    rowIndex++;
	}
	p.add(new Label("Knappsten Sieger"));
	p.add(new Table(dt, Wahlinformationssystem.createTableOptions()));
    
	DataTable verlierer = DataTable.create();
    verlierer.addColumn(ColumnType.NUMBER, "Wahlkreisnr");
    verlierer.addColumn(ColumnType.STRING, "Vorname");
    verlierer.addColumn(ColumnType.STRING, "Name");
    verlierer.addColumn(ColumnType.STRING, "Parteikürzel");
    verlierer.addColumn(ColumnType.NUMBER, "Differenz");
    verlierer.addRows(e.getVerlierer().size());
	rowIndex = 0;
	for (KnappsteVerlierer k : e.getVerlierer()) {
		verlierer.setValue(rowIndex, 0, k.getWahlkreisnr());
		verlierer.setValue(rowIndex, 1, k.getVorname());
		verlierer.setValue(rowIndex, 2, k.getNachname());
		verlierer.setValue(rowIndex, 3, k.getParteikuerzel());
		verlierer.setValue(rowIndex, 4, k.getDifferenz());
		
	    rowIndex++;
	}
	p.add(new Label("Knappsten Verlierer"));
	p.add(new Table(dt, Wahlinformationssystem.createTableOptions()));
	
    }
    
	

}
