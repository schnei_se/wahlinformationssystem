package de.tum.wahl.client.parser;

import java.util.List;

import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.Table;

import de.tum.wahl.client.Wahlinformationssystem;
import de.tum.wahl.shared.Sitzzuteilung;

public class SitzzuteilungParser implements ResultParser<List<Sitzzuteilung>> {

    @Override
    public void parse(List<Sitzzuteilung> e, ComplexPanel p) {
	DataTable dt = DataTable.create();
	dt.addColumn(ColumnType.STRING, "Partei");
	dt.addColumn(ColumnType.STRING, "Name");
	dt.addColumn(ColumnType.STRING, "Vorname");
	dt.addRows(e.size());
	int rowIndex = 0;
	for (Sitzzuteilung zuteilung : e) {
	    dt.setValue(rowIndex, 0, zuteilung.getParteikuerzel());
	    dt.setValue(rowIndex, 1, zuteilung.getName());
	    dt.setValue(rowIndex, 2, zuteilung.getVorname());
	    rowIndex++;
	}
	p.add(new Table(dt, Wahlinformationssystem.createTableOptions()));
    }
}
