package de.tum.wahl.client.parser;

import java.util.List;

import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.corechart.PieChart;

import de.tum.wahl.client.Wahlinformationssystem;
import de.tum.wahl.shared.Sitzverteilung;

final public class SitzverteilungParser implements
        ResultParser<List<Sitzverteilung>> {
    @Override
	public void parse(List<Sitzverteilung> sitzverteilung, ComplexPanel p) {
        DataTable dt = DataTable.create();
        dt.addColumn(ColumnType.STRING, "Kürzel");
        dt.addColumn(ColumnType.NUMBER, "Anzahl Mandate");
        dt.addRows(sitzverteilung.size());
        int rowIndex = 0;
        for (Sitzverteilung sitzzahl : sitzverteilung) {
    	dt.setValue(rowIndex, 0, sitzzahl.getParteikuerzel());
    	dt.setValue(rowIndex, 1, sitzzahl.getAnzahl());
    	rowIndex++;
        }
        
        p.add(new PieChart(dt, Wahlinformationssystem.createPieChartOptions()));
    }
}