package de.tum.wahl.client.parser;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ComplexPanel;


public final class ParsingCallback<E> implements AsyncCallback<E> {
	private final ComplexPanel panel;
	private final ResultParser<E> parser;

	public ParsingCallback(ComplexPanel panel, ResultParser<E> parser) {
	    this.panel = panel;
	    this.parser = parser;
	}

	@Override
	public void onFailure(Throwable caught) {
	    Window.alert("RPC failed. Update");
		throw new RuntimeException(caught);

	}

	@Override
	public void onSuccess(E parseable) {
		panel.clear();
	    parser.parse(parseable, panel);
	}

   }