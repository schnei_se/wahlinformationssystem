package de.tum.wahl.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.tum.wahl.shared.Stimmzettel;
import de.tum.wahl.shared.Vote;

public interface VoteServiceAsync {



	void vote(Vote stimmzettel, AsyncCallback<Void> callback);

	void getStimmzettel(AsyncCallback<Stimmzettel> callback);
	
}
