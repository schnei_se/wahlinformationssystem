package de.tum.wahl.client.presenter;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;

import de.tum.wahl.client.VoteService;
import de.tum.wahl.client.VoteServiceAsync;
import de.tum.wahl.shared.Kandidat;
import de.tum.wahl.shared.Partei;
import de.tum.wahl.shared.Stimmzettel;
import de.tum.wahl.shared.Vote;

public class StimmabgabePresenter {

    public interface Display {
	public void setParteien(List<Partei> p);

	public void setKandidaten(List<Kandidat> k);

	public HasClickHandlers getAbgebenButton();

	public HasValue<String> getTanField();

	public Widget asWidget();

	public Kandidat getErststimme();

	public Partei getZweitstimme();
    }

    private VoteServiceAsync voteService = GWT.create(VoteService.class);
    private Display display;

    public StimmabgabePresenter(Display display) {
	this.display = display;

//	doLoadStimmzettel();

	bind();
    }

    private void bind() {
	display.getAbgebenButton().addClickHandler(new ClickHandler() {

	    @Override
	    public void onClick(ClickEvent event) {
		Kandidat erststimme = display.getErststimme();
		Partei zweitstimme = display.getZweitstimme();

		Vote v = new Vote(erststimme, zweitstimme, display
			.getTanField().getValue());
		if (erststimme != null)
		    v.setErststimme(erststimme);
		if (zweitstimme != null) {
		    v.setZweitstimme(zweitstimme);
		}
		doStimmeAbgeben(v);
	    }

	});
    }

    public void doLoadStimmzettel() {
	voteService.getStimmzettel(new AsyncCallback<Stimmzettel>() {

	    @Override
	    public void onFailure(Throwable caught) {
		Window.alert("Fehler beim Laden des Stimmzettels.");
	    }

	    @Override
	    public void onSuccess(Stimmzettel result) {
		List<Kandidat> kandidaten = result.getDirektkaniddaten();
		List<Partei> parteien = result.getLandeslisten();

		display.setKandidaten(kandidaten);
		display.setParteien(parteien);
	    }

	});
    }

    private void doStimmeAbgeben(Vote vote) {
	voteService.vote(vote, new AsyncCallback<Void>() {

	    @Override
	    public void onFailure(Throwable caught) {
		Window.alert("Fehler beim Abgeben der Stimme: "
			+ caught.getMessage());
	    }

	    @Override
	    public void onSuccess(Void result) {
		Window.alert("Stimmabgabe erfolgreich.");
	    }

	});
    }

}
