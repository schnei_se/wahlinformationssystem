package de.tum.wahl.client;

import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.visualization.client.VisualizationUtils;

import com.google.gwt.visualization.client.formatters.NumberFormat;
import com.google.gwt.visualization.client.visualizations.GeoMap;
import com.google.gwt.visualization.client.visualizations.Table;
import com.google.gwt.visualization.client.visualizations.corechart.CoreChart;
import com.google.gwt.visualization.client.visualizations.corechart.PieChart.PieOptions;

import de.tum.wahl.client.parser.KnappstesiegerParser;
import de.tum.wahl.client.parser.ParsingCallback;
import de.tum.wahl.client.parser.SitzverteilungParser;
import de.tum.wahl.client.parser.SitzzuteilungParser;
import de.tum.wahl.client.parser.UeberhangmandatParser;
import de.tum.wahl.client.parser.WahlkreissiegerParser;
import de.tum.wahl.client.presenter.StimmabgabePresenter;
import de.tum.wahl.client.presenter.WahlkreisuebersichtPresenter;
import de.tum.wahl.client.view.StimmabgabeView;
import de.tum.wahl.client.view.WahlkreisuebersichtView;
import de.tum.wahl.shared.GesamteGewinnerVerlierer;
import de.tum.wahl.shared.Sitzverteilung;
import de.tum.wahl.shared.Sitzzuteilung;
import de.tum.wahl.shared.Ueberhangmandat;
import de.tum.wahl.shared.Wahlkreissieger;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Wahlinformationssystem implements EntryPoint {
    private final TabPanel tabPanel = new TabPanel();
    private final FlowPanel sitzverteilung = new FlowPanel();
    private final FlowPanel sitzzuteilung = new FlowPanel();
    private final FlowPanel wahlkreissieger = new FlowPanel();
    private final FlowPanel ueberhangmandate = new FlowPanel();
    private final FlowPanel knappstesieger = new FlowPanel();
    private final WahlkreisOracle wahlkreisOracle = new WahlkreisOracle();

    private StimmabgabePresenter sp;

    private final Button panicButton = new Button("Panic!");
    private ElectionServiceAsync electionService = (ElectionServiceAsync) GWT
	    .create(ElectionService.class);

    /**
     * This is the entry point method.
     */
    @Override
    public void onModuleLoad() {

	final VerticalPanel vp = new VerticalPanel();
	vp.add(panicButton);
	vp.setCellHorizontalAlignment(panicButton,
		HasHorizontalAlignment.ALIGN_RIGHT);

	VisualizationUtils.loadVisualizationApi(new Runnable() {

	    @Override
	    public void run() {
		StimmabgabeView sv = new StimmabgabeView();
		sp = new StimmabgabePresenter(sv);
		WahlkreisuebersichtView wv = new WahlkreisuebersichtView(
			wahlkreisOracle);
		@SuppressWarnings("unused")
		WahlkreisuebersichtPresenter wp = new WahlkreisuebersichtPresenter(
			electionService, true, wv);
		WahlkreisuebersichtView wverststimmen = new WahlkreisuebersichtView(
			wahlkreisOracle);
		@SuppressWarnings("unused")
		WahlkreisuebersichtPresenter wperstimmen = new WahlkreisuebersichtPresenter(
			electionService, false, wverststimmen);
		vp.getElement().getStyle().setPropertyPx("margin", 15);
		RootPanel.get().add(vp);
		vp.add(new Label("Wahlinformationssystem"));
		vp.add(tabPanel);
		tabPanel.setWidth("1000");
		tabPanel.setHeight("800");
		tabPanel.add(sitzverteilung, "Sitzverteilung");
		tabPanel.add(sitzzuteilung, "Sitzzuteilung");
		tabPanel.add(wv.asWidget(), "Wahlkreisübersicht");
		tabPanel.add(wahlkreissieger, "Wahlkreissieger");
		tabPanel.add(ueberhangmandate, "Überhangmandate");
		tabPanel.add(knappstesieger, "KnappsteSieger");
		tabPanel.add(wverststimmen, "Wahlkreisübersicht");
		tabPanel.add(sv.asWidget(), "Stimmabgabe");

		bind();

		tabPanel.selectTab(0);
	    }
	}, CoreChart.PACKAGE, Table.PACKAGE, GeoMap.PACKAGE);

    }

    private void bind() {
	tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
	    @Override
	    public void onSelection(SelectionEvent<Integer> event) {
		if (event.getSelectedItem() < VIEWNAME.values().length)
		    doLoadAndShowData(VIEWNAME.values()[event.getSelectedItem()]);
	    }
	});

	panicButton.addClickHandler(new ClickHandler() {

	    @Override
	    public void onClick(ClickEvent event) {
		electionService.reset(new AsyncCallback<Void>() {

		    @Override
		    public void onFailure(Throwable caught) {
			Window.alert("!!!!!!!!!!!!!!!! Reset failed !!!!!!!!!!!!!!!!!!!!");
		    }

		    @Override
		    public void onSuccess(Void result) {
			Window.alert("Reset war erfolgreich :)");
		    }

		});
	    }

	});
    }

    private enum VIEWNAME {
	Sitzverteilung, Sitzzuteilung, Wahlkreisuebersicht, Wahlkreissieger, Ueberhangmandate, KnappsteSieger, WahlkreisuebersichtZweitstimmen, Stimmabgabe
    }

    private void doLoadAndShowData(VIEWNAME view) {
	switch (view) {
	case Sitzverteilung:
	    electionService
		    .getSitzverteilung(new ParsingCallback<List<Sitzverteilung>>(
			    sitzverteilung, new SitzverteilungParser()));
	    break;
	case Sitzzuteilung:
	    electionService
		    .getSitzzuteilung(new ParsingCallback<List<Sitzzuteilung>>(
			    sitzzuteilung, new SitzzuteilungParser()));
	    break;
	case Wahlkreisuebersicht:
	    // wahlkreisuebersicht.add(new SuggestBox(wahlkreisOracle));
	    // electionService
	    // .getWahlkreisuebersicht(new
	    // ParsingCallback<List<Wahlkreisuebersicht>>(
	    // wahlkreisuebersicht,
	    // new WahlkreisuebersichtParser()));
	    // ParsingCallback pc = new
	    // ParsingCallback<List<Wahlkreisuebersicht>>(wahlkreisuebersicht,
	    // new WahlkreisuebersichtParser());
	    break;
	case Wahlkreissieger:
	    electionService
		    .getWahlkreissieger(new ParsingCallback<List<Wahlkreissieger>>(
			    wahlkreissieger, new WahlkreissiegerParser()));
	    break;
	case Ueberhangmandate:
	    electionService
		    .getUeberhangmandate(new ParsingCallback<List<Ueberhangmandat>>(
			    ueberhangmandate, new UeberhangmandatParser()));
	    break;
	case KnappsteSieger:
	    electionService
		    .getKnappsteGewinnerVerlierer(new ParsingCallback<GesamteGewinnerVerlierer>(
			    knappstesieger, new KnappstesiegerParser()));
	    break;
	case WahlkreisuebersichtZweitstimmen:
	    break;
	case Stimmabgabe:
	    sp.doLoadStimmzettel();
	    break;
	default:
	    throw new IllegalArgumentException("Unbekannte View: "
		    + view.toString());
	}
    }

    /**
     * Creates the options for a PieChart
     * 
     * @return
     */
    public static PieOptions createPieChartOptions() {
	PieOptions options = PieOptions.create();
	options.setWidth(600);
	options.setHeight(240);
	return options;
    }

    /**
     * Creates the options for a Table
     * 
     * @return
     */
    public static Table.Options createTableOptions() {
	Table.Options options = Table.Options.create();
	options.setPageSize(150);
	return options;
    }

    public static NumberFormat createFractionOptions() {
	NumberFormat.Options options = NumberFormat.Options.create();
	options.setFractionDigits(2);
	NumberFormat nf = NumberFormat.create(options);
	return nf;
    }

    /**
     * Creates the options for a Map
     * 
     * @return
     */
    public static GeoMap.Options createGeoMapOptions() {
	GeoMap.Options options = GeoMap.Options.create();
	options.set("title",
		"Anzahl Überhangmandate nach Bundesländern und Parteien");
	options.setWidth(650);
	options.setHeight(400);
	options.set("region", "DE");
	options.set("resolution", "provices");
	options.set("showLegend", false);
	options.setColors(3);
	return options;
    }

}
