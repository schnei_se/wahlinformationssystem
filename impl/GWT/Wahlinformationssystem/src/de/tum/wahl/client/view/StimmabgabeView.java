package de.tum.wahl.client.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.tum.wahl.client.presenter.StimmabgabePresenter;
import de.tum.wahl.shared.Kandidat;
import de.tum.wahl.shared.Partei;

public class StimmabgabeView extends Composite implements StimmabgabePresenter.Display{

	private int kandidatenNr;
	private int parteienNr;
	private VerticalPanel vp;
	private FlexTable table;
	private Button abgebenButton;
	private TextBox tanTextBox;
	private List<CheckBox> kandidatenCB;
	private List<CheckBox> parteienCB;
	private List<Kandidat> kandidaten;
	private List<Partei> parteien;
	
	public StimmabgabeView(){
		vp = new VerticalPanel();
		table = new FlexTable();
		abgebenButton = new Button("Abgeben");
		tanTextBox = new TextBox();
		table.setTitle("Stimmzettel");
		table.setText(0, 0, "Nr");
		table.setText(0, 1, "Kandidat");
		table.setText(0, 2, "Erststimme");
		table.setText(0, 3, "Zweitstimme");
		table.setText(0, 4, "Partei");
		table.setText(0, 5, "Nr");
		vp.add(table);
		vp.add(new Label("TAN:"));
		vp.add(tanTextBox);
		vp.add(abgebenButton);
		
		kandidatenNr = 1;
		parteienNr = 1;
		
		kandidatenCB = new ArrayList<CheckBox>();
		parteienCB = new ArrayList<CheckBox>();
		initWidget(vp);
	}
	
	
	
	@Override
	public void setParteien(List<Partei> parteien) {
	    	this.parteien = parteien;
		for(Partei p: parteien){
			CheckBox cb = new CheckBox();
			cb.setFormValue(p.getKuerzel());
			table.setWidget(parteienNr, 3, cb);
			table.setText(parteienNr, 4, p.getKuerzel()+" "+p.getName());
			table.setText(parteienNr, 5, parteienNr+"");
			parteienCB.add(cb);
			parteienNr++;
		}
	}

	@Override
	public void setKandidaten(List<Kandidat> kandidaten) {
	    	this.kandidaten = kandidaten;
		for(Kandidat k: kandidaten){
			CheckBox cb = new CheckBox();
			cb.setFormValue(k.getName()+k.getVorname());
			table.setText(kandidatenNr, 0, kandidatenNr+"");
			table.setText(kandidatenNr, 1, k.getTitel()+" "+k.getVorname()+" "+k.getName()+" "+k.getJahrgang());
			table.setWidget(kandidatenNr, 2, cb);
			kandidatenCB.add(cb);
			kandidatenNr++;
		}
	}

	@Override
	public HasClickHandlers getAbgebenButton() {
		return abgebenButton;
	}



	@Override
	public HasValue<String> getTanField() {
		return tanTextBox;
	}



	



	@Override
	public Kandidat getErststimme() {
		for(int i = 0; i < kandidatenCB.size(); i++){
			CheckBox cb = kandidatenCB.get(i);
			if(cb.getValue())
				return kandidaten.get(i);
		}
		return null;
	}



	@Override
	public Partei getZweitstimme() {
		for(int i = 0; i < parteienCB.size(); i++){
		CheckBox cb = parteienCB.get(i);
		if(cb.getValue())
			return parteien.get(i);
	}
	return null;
	}



	
	
	
}
