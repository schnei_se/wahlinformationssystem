package de.tum.wahl.client.view;

import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

import de.tum.wahl.client.presenter.WahlkreisuebersichtPresenter;

public class WahlkreisuebersichtView extends Composite implements WahlkreisuebersichtPresenter.Display {
	
	private VerticalPanel vp;
	private FlowPanel dataPanel;
	private SuggestBox suggestBox;
	public WahlkreisuebersichtView(MultiWordSuggestOracle mwso){
		vp = new VerticalPanel();
		HorizontalPanel hp = new HorizontalPanel();
		
		dataPanel = new FlowPanel();
		suggestBox =new SuggestBox(mwso);
		hp.add(new Label("Wahlkreis eingeben:"));
		hp.add(suggestBox);
		vp.add(hp);
		vp.add(dataPanel);
		initWidget(vp);

	}
	
	
	
	@Override
	public HasSelectionHandlers<Suggestion> getSugestBox() {
		return suggestBox;
	}



	@Override
	public FlowPanel getDataPanel() {
		return dataPanel;
	}

}
