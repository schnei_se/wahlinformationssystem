package de.tum.wahl.client.parser;

import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.Table;

import de.tum.wahl.client.Wahlinformationssystem;
import de.tum.wahl.shared.Direktkandidat;
import de.tum.wahl.shared.GesamteWahlkreisuebersicht;
import de.tum.wahl.shared.Wahlkreisuebersicht;

public class WahlkreisuebersichtParser implements
	ResultParser<GesamteWahlkreisuebersicht> {

    @Override
    public void parse(GesamteWahlkreisuebersicht e, ComplexPanel p) {
	
    	DataTable dt2 = DataTable.create();
    	dt2.addColumn(ColumnType.STRING, "Vorname");
    	dt2.addColumn(ColumnType.STRING, "Nachname");
    	dt2.addColumn(ColumnType.STRING, "Parteikuerzel");
    	dt2.addColumn(ColumnType.NUMBER, "Jahr");
    	dt2.addColumn(ColumnType.NUMBER, "Wahlbeteiligung [%]");
    	dt2.addRows(e.getKandidaten().size());
    	int rowIndex = 0;
    	for(Direktkandidat d: e.getKandidaten()){    		
    		dt2.setValue(rowIndex, 0, d.getVorname());
    		dt2.setValue(rowIndex, 1, d.getName());
    		dt2.setValue(rowIndex, 2, d.getParteikuerzel());
    		dt2.setValue(rowIndex, 3, d.getJahr());
    		dt2.setValue(rowIndex, 4, d.getWahlbeteiligung());
    		rowIndex++;
    	}
   
   rowIndex = 0;
    DataTable dt = DataTable.create();
	dt.addColumn(ColumnType.STRING, "Partei");
	dt.addColumn(ColumnType.NUMBER, "Jahr");
	dt.addColumn(ColumnType.NUMBER, "Stimmen [%]");
	dt.addColumn(ColumnType.NUMBER, "Stimmen [absolut]");
	dt.addRows(e.getStimmen().size());
	 rowIndex = 0;
	for (Wahlkreisuebersicht w : e.getStimmen()) {
	    dt.setValue(rowIndex, 0, w.getParteikuerzel());
	    dt.setValue(rowIndex, 1, w.getJahr());
	    dt.setValue(rowIndex, 2, w.getRelativ());
	    dt.setValue(rowIndex, 3, w.getAbsolut());
	    rowIndex++;
	}
	p.add(new Label("Direktkandidaten und Wahlbeteiligung im Wahlkreis "+ e.getKandidaten().get(0).getWahlkreisnr()));
	Wahlinformationssystem.createFractionOptions().format(dt, 2);
	Wahlinformationssystem.createFractionOptions().format(dt2, 4);
	
	p.add(new Table(dt2, Wahlinformationssystem.createTableOptions()));
	p.add(new Label("Zweitstimmen pro Partei im Wahlkreis "+ e.getKandidaten().get(0).getWahlkreisnr()));
	p.add(new Table(dt, Wahlinformationssystem.createTableOptions()));

	
	
    }
    
}
