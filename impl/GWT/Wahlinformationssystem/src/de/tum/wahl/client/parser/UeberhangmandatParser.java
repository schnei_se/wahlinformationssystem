package de.tum.wahl.client.parser;

import java.util.List;

import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.GeoMap;
import com.google.gwt.visualization.client.visualizations.Table;

import de.tum.wahl.client.Wahlinformationssystem;
import de.tum.wahl.shared.Ueberhangmandat;

public class UeberhangmandatParser implements
	ResultParser<List<Ueberhangmandat>> {

    @Override
    public void parse(List<Ueberhangmandat> e, ComplexPanel p) {
	DataTable dt = DataTable.create();
	dt.addColumn(ColumnType.STRING, "Land");
	dt.addColumn(ColumnType.STRING, "Partei und Anzahl Überhangmandate");
	dt.addRows(e.size());
	int rowIndex = 0;
	for (Ueberhangmandat u : e) {
	    dt.setValue(rowIndex, 0, u.getLandname());
	    dt.setValue(rowIndex, 1, u.getParteikuerzel()+" "+ u.getAnzahl());
	    rowIndex++;
	}
	p.add(new GeoMap(dt, Wahlinformationssystem.createGeoMapOptions()));
	p.add(new Table(dt, Wahlinformationssystem.createTableOptions()));
    }

}
