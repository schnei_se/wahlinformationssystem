package de.tum.wahl.client.parser;

import com.google.gwt.user.client.ui.ComplexPanel;

interface ResultParser<E> {
    void parse(E e, ComplexPanel p);
}