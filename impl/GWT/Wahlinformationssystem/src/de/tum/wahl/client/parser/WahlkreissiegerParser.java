package de.tum.wahl.client.parser;

import java.util.List;

import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.visualizations.Table;

import de.tum.wahl.client.Wahlinformationssystem;
import de.tum.wahl.shared.Wahlkreissieger;

public class WahlkreissiegerParser implements
	ResultParser<List<Wahlkreissieger>> {

    @Override
    public void parse(List<Wahlkreissieger> e, ComplexPanel p) {
	DataTable dt = DataTable.create();
	dt.addColumn(ColumnType.STRING, "Wahlkreis");
	dt.addColumn(ColumnType.STRING, "Parteikürzel");
	dt.addColumn(ColumnType.STRING, "Name");
	dt.addRows(e.size());
	int rowIndex = 0;
	for (Wahlkreissieger w : e) {
	    dt.setValue(rowIndex, 0, w.getWahlkreisname());
	    dt.setValue(rowIndex, 1, w.getParteikuerzel());
	    dt.setValue(rowIndex, 2, w.getKandidatname());
	    rowIndex++;
	}
	p.add(new Table(dt, Wahlinformationssystem.createTableOptions()));
    }

}
