package de.tum.wahl.shared;

import java.io.Serializable;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Stimmzettel implements Serializable, IsSerializable {
    private static final long serialVersionUID = 1L;

    List<Kandidat> direktkaniddaten;
    List<Partei> landeslisten;
    int wahlkreis;

    public Stimmzettel() {

    }

    public Stimmzettel(List<Kandidat> direktkaniddaten,
	    List<Partei> landeslisten, int wahlkreis) {
	super();
	this.direktkaniddaten = direktkaniddaten;
	this.landeslisten = landeslisten;
	this.wahlkreis = wahlkreis;
    }

    public List<Kandidat> getDirektkaniddaten() {
	return direktkaniddaten;
    }

    public void setDirektkaniddaten(List<Kandidat> direktkaniddaten) {
	this.direktkaniddaten = direktkaniddaten;
    }

    public List<Partei> getLandeslisten() {
	return landeslisten;
    }

    public void setLandeslisten(List<Partei> landeslisten) {
	this.landeslisten = landeslisten;
    }

    public int getWahlkreis() {
	return wahlkreis;
    }

}
