package de.tum.wahl.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class VoteException extends Exception implements IsSerializable {
    private static final long serialVersionUID = 1L;

    public VoteException() {
	super();
    }

    public VoteException(String msg) {
	super(msg);
    }
}
