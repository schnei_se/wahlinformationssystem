package de.tum.wahl.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class KnappsteVerlierer implements IsSerializable, Serializable {
	
	private Integer id;
	private String vorname;
	private String name;
	private int wahlkreisnr;
	private int differenz;
	private String parteikuerzel;
	
	
	public KnappsteVerlierer(){
		
		
	}
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return name;
	}
	public void setNachname(String nachname) {
		name = nachname;
	}
	public int getWahlkreisnr() {
		return wahlkreisnr;
	}
	public void setWahlkreisnr(int wahlkreisnr) {
		this.wahlkreisnr = wahlkreisnr;
	}
	public int getDifferenz() {
		return differenz;
	}
	public void setDifferenz(int differenz) {
		this.differenz = differenz;
	}
	public String getParteikuerzel() {
		return parteikuerzel;
	}
	public void setParteikuerzel(String parteikuerzel) {
		this.parteikuerzel = parteikuerzel;
	}
	
	
	
	
}
