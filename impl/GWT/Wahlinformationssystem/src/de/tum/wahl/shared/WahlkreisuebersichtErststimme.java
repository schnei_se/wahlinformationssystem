package de.tum.wahl.shared;

import java.io.Serializable;


import com.google.gwt.user.client.rpc.IsSerializable;

public class WahlkreisuebersichtErststimme implements IsSerializable, Serializable{
    private static final long serialVersionUID = 1L;

	private Integer nr;
	private Integer wahlkreisnr;
	private String parteikuerzel;
	private Integer jahr;
	private Double relativ;
	private Integer absolut;
	
	public WahlkreisuebersichtErststimme(){
		
	}
	
	public Integer getNr() {
		return nr;
	}
	public void setNr(Integer nr) {
		this.nr = nr;
	}
	public Integer getWahlkreisnr() {
		return wahlkreisnr;
	}
	public void setWahlkreisnr(Integer wahlkreisnr) {
		this.wahlkreisnr = wahlkreisnr;
	}
	public String getParteikuerzel() {
		return parteikuerzel;
	}
	public void setParteikuerzel(String parteikuerzel) {
		this.parteikuerzel = parteikuerzel;
	}
	public Integer getJahr() {
		return jahr;
	}
	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}
	public Double getRelativ() {
		return relativ;
	}
	public void setRelativ(Double relativ) {
		this.relativ = relativ;
	}
	public Integer getAbsolut() {
		return absolut;
	}
	public void setAbsolut(Integer absolut) {
		this.absolut = absolut;
	}
	
	
}
