package de.tum.wahl.shared;

import java.io.Serializable;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The client class for the sitzverteilung database view.
 */
public class Sitzverteilung implements Serializable, IsSerializable {

    private static final long serialVersionUID = -8053290650330336822L;

    private Integer anzahl;

    private String parteikuerzel;

    public Sitzverteilung() {
    }

    public Integer getAnzahl() {
	return this.anzahl;
    }

    public String getParteikuerzel() {
	return this.parteikuerzel;
    }

}