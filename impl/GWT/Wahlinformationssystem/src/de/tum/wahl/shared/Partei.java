package de.tum.wahl.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Partei implements Serializable, IsSerializable {
    private static final long serialVersionUID = 1L;

    private String kuerzel;
    private String name;

    public Partei() {

    }

    public Partei(String kuerzel, String name) {
	this.kuerzel = kuerzel;
	this.name = name;
    }

    public String getKuerzel() {
	return kuerzel;
    }

    public void setKuerzel(String kuerzel) {
	this.kuerzel = kuerzel;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

}
