package de.tum.wahl.shared;

import java.io.Serializable;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The persistent class for the ueberhangmandate database table.
 * 
 */
// @Entity
// @Table(name="ueberhangmandate")
public class Ueberhangmandat implements Serializable, IsSerializable {
    private static final long serialVersionUID = 1L;

    private Integer anzahl;

    private String landname;

    private String parteikuerzel;

    public Ueberhangmandat() {
    }

    public Integer getAnzahl() {
	return this.anzahl;
    }

    public void setAnzahl(Integer anzahl) {
	this.anzahl = anzahl;
    }

    public String getLandname() {
	return this.landname;
    }

    public void setLandname(String landname) {
	this.landname = landname;
    }

    public String getParteikuerzel() {
	return this.parteikuerzel;
    }

    public void setParteikuerzel(String parteikuerzel) {
	this.parteikuerzel = parteikuerzel;
    }

}