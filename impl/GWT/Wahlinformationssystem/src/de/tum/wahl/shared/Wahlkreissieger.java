package de.tum.wahl.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The persistent class for the wahlkreissieger database table.
 * 
 */
// @Entity
public class Wahlkreissieger implements Serializable, IsSerializable {
    private static final long serialVersionUID = 1L;

    private String kandidatname;

    private String parteikuerzel;

    private String wahlkreisname;

    public Wahlkreissieger() {
    }

    public String getKandidatname() {
	return this.kandidatname;
    }

    public void setKandidatname(String kandidatname) {
	this.kandidatname = kandidatname;
    }

    public String getParteikuerzel() {
	return this.parteikuerzel;
    }

    public void setParteikuerzel(String parteikuerzel) {
	this.parteikuerzel = parteikuerzel;
    }

    public String getWahlkreisname() {
	return this.wahlkreisname;
    }

    public void setWahlkreisname(String wahlkreisname) {
	this.wahlkreisname = wahlkreisname;
    }

}