package de.tum.wahl.shared;

import java.io.Serializable;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The persistent class for the knappstesieger database table.
 * 
 */
public class Knappstesieger implements Serializable, IsSerializable {
    private static final long serialVersionUID = 1L;
    
    private Integer id;

    private Integer vorsprung;

    private String parteikuerzel;

    private String name;
    private String vorname;
    

    private Integer wahlkreisnr;

    public Knappstesieger() {
    }
    
    public Integer getId(){
    	return id;
    }
    public void setId(Integer id){
    	this.id = id;
    }
	public Integer getVorsprung() {
		return vorsprung;
	}

	public void setVorsprung(Integer vorsprung) {
		this.vorsprung = vorsprung;
	}

	public String getParteikuerzel() {
		return parteikuerzel;
	}

	public void setParteikuerzel(String parteikuerzel) {
		this.parteikuerzel = parteikuerzel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public Integer getWahlkreisnr() {
		return wahlkreisnr;
	}

	public void setWahlkreisnr(Integer wahlkreisnr) {
		this.wahlkreisnr = wahlkreisnr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}