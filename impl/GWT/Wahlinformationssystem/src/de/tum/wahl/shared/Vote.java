package de.tum.wahl.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Vote implements Serializable, IsSerializable {
    private static final long serialVersionUID = 1L;

    private Kandidat erststimme;
    private Partei zweitstimme;
    private String tan;

    public Vote() {

    }

    public Vote(Kandidat erststimme, Partei zweitstimme, String tan) {
	super();
	this.erststimme = erststimme;
	this.zweitstimme = zweitstimme;
	this.tan = tan;
    }

    public Kandidat getErststimme() {
	return erststimme;
    }

    public void setErststimme(Kandidat erststimme) {
	this.erststimme = erststimme;
    }

    public Partei getZweitstimme() {
	return zweitstimme;
    }

    public void setZweitstimme(Partei zweitstimme) {
	this.zweitstimme = zweitstimme;
    }

    public String getTan() {
	return tan;
    }

}
