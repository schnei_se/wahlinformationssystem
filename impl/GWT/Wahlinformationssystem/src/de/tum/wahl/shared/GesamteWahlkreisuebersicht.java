package de.tum.wahl.shared;

import java.io.Serializable;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class GesamteWahlkreisuebersicht implements Serializable, IsSerializable{
	private static final long serialVersionUID = 1L;
	private List<Direktkandidat> kandidaten;
	private List<Wahlkreisuebersicht> stimmen;
	
	public GesamteWahlkreisuebersicht(){
		
	}

	public List<Direktkandidat> getKandidaten() {
		return kandidaten;
	}

	public void setKandidaten(List<Direktkandidat> kandidaten) {
		this.kandidaten = kandidaten;
	}

	public List<Wahlkreisuebersicht> getStimmen() {
		return stimmen;
	}

	public void setStimmen(List<Wahlkreisuebersicht> stimmen) {
		this.stimmen = stimmen;
	}
	
	
}
