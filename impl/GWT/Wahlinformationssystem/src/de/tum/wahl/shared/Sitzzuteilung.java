package de.tum.wahl.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The persistent class for the sitzzuteilung database table.
 * 
 */
public class Sitzzuteilung implements Serializable, IsSerializable {
    private static final long serialVersionUID = 1L;

    private Integer jahrgang;

    private Integer kandidatnr;

    private String name;

    private String parteikuerzel;

    private String vorname;

    public Sitzzuteilung() {
    }

    public Integer getJahrgang() {
	return this.jahrgang;
    }

    public void setJahrgang(Integer jahrgang) {
	this.jahrgang = jahrgang;
    }

    public Integer getKandidatnr() {
	return this.kandidatnr;
    }

    public void setKandidatnr(Integer kandidatnr) {
	this.kandidatnr = kandidatnr;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getParteikuerzel() {
	return this.parteikuerzel;
    }

    public void setParteikuerzel(String parteikuerzel) {
	this.parteikuerzel = parteikuerzel;
    }

    public String getVorname() {
	return this.vorname;
    }

    public void setVorname(String vorname) {
	this.vorname = vorname;
    }

}