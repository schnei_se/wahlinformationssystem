package de.tum.wahl.shared;

import java.io.Serializable;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class GesamteGewinnerVerlierer implements IsSerializable, Serializable {
	
	
	private List<KnappsteVerlierer> verlierer;
	private List<Knappstesieger> sieger;
	
	public List<KnappsteVerlierer> getVerlierer() {
		return verlierer;
	}
	public void setVerlierer(List<KnappsteVerlierer> verlierer) {
		this.verlierer = verlierer;
	}
	public List<Knappstesieger> getSieger() {
		return sieger;
	}
	public void setSieger(List<Knappstesieger> sieger) {
		this.sieger = sieger;
	}
	
	
}
