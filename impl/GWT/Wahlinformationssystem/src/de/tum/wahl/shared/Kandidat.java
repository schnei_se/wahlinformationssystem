package de.tum.wahl.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Kandidat implements Serializable, IsSerializable {
    private static final long serialVersionUID = 1L;

    private int nr;
    private String titel;
    private String vorname;
    private String name;
    private int jahrgang;
    private String parteiKuerzel;

    public Kandidat() {

    }

    public Kandidat(int nr, String titel, String vorname, String name,
	    int jahrgang, String parteiKuerzel) {
	super();
	this.titel = titel;
	this.vorname = vorname;
	this.name = name;
	this.jahrgang = jahrgang;
	this.parteiKuerzel = parteiKuerzel;
    }

    public int getNr() {
	return nr;
    }

    public void setNr(int nr) {
	this.nr = nr;
    }

    public String getTitel() {
	return titel;
    }

    public void setTitel(String titel) {
	this.titel = titel;
    }

    public String getVorname() {
	return vorname;
    }

    public void setVorname(String vorname) {
	this.vorname = vorname;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public int getJahrgang() {
	return jahrgang;
    }

    public void setJahrgang(int jahrgang) {
	this.jahrgang = jahrgang;
    }

    public String getPartei() {
	return parteiKuerzel;
    }

    public void setPartei(String parteiKuerzel) {
	this.parteiKuerzel = parteiKuerzel;
    }

}
