package de.tum.wahl.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The persistent class for the wahlkreisuebersicht database table.
 * 
 */
// @Entity
public class Wahlkreisuebersicht implements Serializable, IsSerializable {
    private static final long serialVersionUID = 1L;

    private Integer nr;
    private Integer absolut;
    private Integer jahr;
    private Double relativ;


    private String parteikuerzel;

   

    private Integer wahlkreisnr;

    public Wahlkreisuebersicht() {
    }
    
    public Integer getJahr(){
    	return jahr;
    }
    
    public void setJahr(Integer jahr){
    	this.jahr = jahr;
    }
    
    public Integer getNr(){
    	return nr;
    }
    public void setNr(Integer nr){
    	this.nr = nr;
    }
	public Integer getAbsolut() {
		return absolut;
	}

	public void setAbsolut(Integer absolut) {
		this.absolut = absolut;
	}

	public Double getRelativ() {
		return relativ;
	}

	public void setRelativ05(Double relativ) {
		this.relativ = relativ;
	}

	public String getParteikuerzel() {
		return parteikuerzel;
	}

	public void setParteikuerzel(String parteikuerzel) {
		this.parteikuerzel = parteikuerzel;
	}


	public Integer getWahlkreisnr() {
		return wahlkreisnr;
	}

	public void setWahlkreisnr(Integer wahlkreisnr) {
		this.wahlkreisnr = wahlkreisnr;
	}




}