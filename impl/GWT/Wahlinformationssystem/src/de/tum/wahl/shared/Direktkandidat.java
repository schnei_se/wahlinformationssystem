package de.tum.wahl.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Direktkandidat implements Serializable, IsSerializable{

	private static final long serialVersionUID = 1L;
	
	private Integer nr;
	private Integer wahlkreisnr;
	private String vorname;
	private String name;
	private String parteikuerzel;
	private Integer jahr;
	private Double wahlbeteiligung;
	
	public Direktkandidat(){
		
	}

	public Integer getNr() {
		return nr;
	}

	public void setNr(Integer nr) {
		this.nr = nr;
	}

	public Integer getWahlkreisnr() {
		return wahlkreisnr;
	}

	public void setWahlkreisnr(Integer wahlkreisnr) {
		this.wahlkreisnr = wahlkreisnr;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParteikuerzel() {
		return parteikuerzel;
	}

	public void setParteikuerzel(String parteikuerzel) {
		this.parteikuerzel = parteikuerzel;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}

	public Double getWahlbeteiligung() {
		return wahlbeteiligung;
	}

	public void setWahlbeteiligung(Double wahlbeteiligung) {
		this.wahlbeteiligung = wahlbeteiligung;
	}
	

	
}
