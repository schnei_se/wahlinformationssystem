package de.tum.wahl.server;

import java.security.Principal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.tum.wahl.client.VoteService;
import de.tum.wahl.shared.Kandidat;
import de.tum.wahl.shared.Partei;
import de.tum.wahl.shared.Stimmzettel;
import de.tum.wahl.shared.Vote;
import de.tum.wahl.shared.VoteException;

/**
 * @author Alexander Ried
 * @author Sebastian Schneider
 * 
 */
public class VoteServiceImpl extends RemoteServiceServlet implements
	VoteService {
    private static final long serialVersionUID = 1L;

    /**
     * Reads the wahlkreisNr from the supplied certificate or throws a
     * voteexception if an error occurs.
     * 
     * @throws VoteException
     */
    private int getWahlkreis() throws VoteException {
	// sicherstellen dass ein zertifikat bereit gestellt wurde
	final HttpServletRequest httpServletRequest = perThreadRequest.get();
	if (!HttpServletRequest.CLIENT_CERT_AUTH.equals(httpServletRequest
		.getAuthType())) {
	    throw new VoteException("Kein Client-Zertifikat übergeben.");
	}

	// wahlkreis aus zertifikat auslesen
	final Principal userPrincipal = httpServletRequest.getUserPrincipal();
	Pattern principalPattern = Pattern
		.compile("(?:.*, )?CN=(\\d+)(?:, .*)?");
	final Matcher matcher = principalPattern.matcher(userPrincipal
		.getName());
	if (!matcher.matches()) {
	    throw new VoteException(
		    "Zertifikat enthält keine Wahlkreisinformationen.");
	}

	return Integer.parseInt(matcher.group(1));
    }

    @Override
    public Stimmzettel getStimmzettel() throws VoteException {
	final int wahlkreis = getWahlkreis();

	Connection conn = Database.getInstance().getConnection();

	List<Kandidat> kandidaten = new ArrayList<Kandidat>();
	List<Partei> parteien = new ArrayList<Partei>();

	try {
	    PreparedStatement psKandidaten = conn
		    .prepareStatement("SELECT DISTINCT k.nr, k.Titel, k.Vorname, k.Name, k.Jahrgang, k.ParteiKuerzel "
			    + "FROM Kandidaten k, Wahlkreise w, kandidiertIn ki "
			    + "WHERE k.nr = ki.KandidatNr AND "
			    + "ki.WahlkreisNr = ?");
	    psKandidaten.setInt(1, wahlkreis);
	    ResultSet rs = psKandidaten.executeQuery();
	    while (rs.next()) {
		Kandidat k = new Kandidat(rs.getInt("nr"),
			rs.getString("Titel"), rs.getString("Vorname"),
			rs.getString("Name"), rs.getInt("Jahrgang"),
			rs.getString("ParteiKuerzel"));
		System.out.println(k);
		kandidaten.add(k);

	    }

	    PreparedStatement psLandeslisten = conn
		    .prepareStatement("SELECT p.Kuerzel, p.Name "
			    + "FROM Parteien p, Landeslisten l, Wahlkreise w "
			    + "WHERE w.Nr = ? AND w.landnr = l.landnr AND l.jahr = 2009 AND l.ParteiKuerzel = p.Kuerzel");
	    psLandeslisten.setInt(1, wahlkreis);

	    ResultSet rs2 = psLandeslisten.executeQuery();
	    while (rs2.next()) {
		String kuerzel = rs2.getString("Kuerzel");
		String name = rs2.getString("Name");
		Partei p = new Partei(kuerzel, name);
		parteien.add(p);
	    }
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new VoteException(e.getMessage());
	}

	Stimmzettel z = new Stimmzettel(kandidaten, parteien, wahlkreis);
	return z;
    }

    @Override
    public void vote(Vote stimmzettel) throws VoteException {
	final int wahlkreis = getWahlkreis();

	final Connection conn = Database.getInstance().getConnection();
	final Integer tan = Integer.valueOf(stimmzettel.getTan());
	try {
	    conn.setAutoCommit(false);
	    PreparedStatement psTAN = conn
		    .prepareStatement("SELECT COUNT(*) FROM Tan WHERE tan = ?");
	    psTAN.setInt(1, tan);
	    final ResultSet rsTAN = psTAN.executeQuery();
	    if (!rsTAN.next())
		throw new VoteException("Fehler im Tan-Query.");
	    int tans = rsTAN.getInt(1);
	    if (tans == 0) {
		// tan ungültig
		throw new VoteException("Tan ungültig.");
	    }

	    PreparedStatement psRemoveTan = conn
		    .prepareStatement("DELETE FROM Tan WHERE tan = ?");
	    psRemoveTan.setInt(1, tan);
	    psRemoveTan.execute();
	    if (psRemoveTan.getUpdateCount() == 0)
		throw new VoteException("Kann Tan nicht löschen");

	    final Kandidat erststimme = stimmzettel.getErststimme();
	    if (erststimme != null) {
		PreparedStatement psInsertErststimme = conn
			.prepareStatement("INSERT INTO Erststimmen VALUES (?, ?, 2009, 1)");
		psInsertErststimme.setInt(1, erststimme.getNr());
		psInsertErststimme.setInt(2, getWahlkreis());
		psInsertErststimme.execute();
		if (psInsertErststimme.getUpdateCount() != 1)
		    throw new VoteException("Kann Erststimme nicht einfügen");
	    }

	    final Partei zweitstimme = stimmzettel.getZweitstimme();
	    if (zweitstimme != null) {
		PreparedStatement psInsertZweitstimme = conn
			.prepareStatement("INSERT INTO Zweitstimmen VALUES ("
				+ "(SELECT l.nr FROM Landeslisten l, Wahlkreise w WHERE l.landnr = w.landnr AND l.parteikuerzel = ? AND w.nr = ?),"
				+ " ?, 2009, 1)");
		psInsertZweitstimme.setString(1, zweitstimme.getKuerzel());
		psInsertZweitstimme.setInt(2, wahlkreis);
		psInsertZweitstimme.setInt(3, wahlkreis);
		psInsertZweitstimme.execute();
		if (psInsertZweitstimme.getUpdateCount() != 1)
		    throw new VoteException("Kann Erststimme nicht einfügen");
	    }

	    conn.commit();
	} catch (SQLException e) {
	    e.printStackTrace();
	    throw new VoteException(e.getMessage());
	} catch (VoteException e) {
	    try {
		conn.rollback();
	    } catch (SQLException e1) {
		e1.printStackTrace();
	    }
	    throw e;
	}

    }

}
