/**
 * 
 */
package de.tum.wahl.server;

import java.io.File;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.tum.wahl.client.ElectionService;
import de.tum.wahl.shared.Direktkandidat;
import de.tum.wahl.shared.GesamteGewinnerVerlierer;
import de.tum.wahl.shared.GesamteWahlkreisuebersicht;
import de.tum.wahl.shared.KnappsteVerlierer;
import de.tum.wahl.shared.Knappstesieger;
import de.tum.wahl.shared.Sitzverteilung;
import de.tum.wahl.shared.Sitzzuteilung;
import de.tum.wahl.shared.Ueberhangmandat;
import de.tum.wahl.shared.Wahlkreissieger;
import de.tum.wahl.shared.Wahlkreisuebersicht;
import de.tum.wahl.shared.WahlkreisuebersichtErststimme;

/**
 * @author Alexander Ried
 * @author Sebastian Schneider
 * 
 */
public class ElectionServiceImpl extends RemoteServiceServlet implements
		ElectionService {
	private static final long serialVersionUID = 1L;

	@PersistenceContext
	EntityManager em;

	@Override
	public List<Sitzverteilung> getSitzverteilung() {
		final List<Sitzverteilung> svs = em.createQuery(
				"SELECT s FROM Sitzverteilung s", Sitzverteilung.class)
				.getResultList();
		return svs;
	}

	@Override
	public List<Sitzzuteilung> getSitzzuteilung() {
		final List<Sitzzuteilung> resultList = em.createQuery(
				"SELECT s from Sitzzuteilung s", Sitzzuteilung.class)
				.getResultList();
		return resultList;
	}

	@Override
	public GesamteWahlkreisuebersicht getWahlkreisuebersicht(int wahlkreisnr) {
		final TypedQuery<Wahlkreisuebersicht> q = em.createQuery(
				"SELECT s from Wahlkreisuebersicht s WHERE s.wahlkreisnr = ?1",
				Wahlkreisuebersicht.class);
		q.setParameter(1, wahlkreisnr);
		final List<Wahlkreisuebersicht> resultList = q.getResultList();

		final TypedQuery<Direktkandidat> q2 = em.createQuery(
				"SELECT d FROM Direktkandidat d WHERE d.wahlkreisnr = ?1",
				Direktkandidat.class);
		q2.setParameter(1, wahlkreisnr);
		final List<Direktkandidat> direktkandidaten = q2.getResultList();
		GesamteWahlkreisuebersicht gw = new GesamteWahlkreisuebersicht();
		gw.setStimmen(resultList);
		gw.setKandidaten(direktkandidaten);
		return gw;
	}

	@Override
	public List<Wahlkreissieger> getWahlkreissieger() {
		final List<Wahlkreissieger> resultList = em.createQuery(
				"SELECT s from Wahlkreissieger s", Wahlkreissieger.class)
				.getResultList();
		return resultList;
	}

	@Override
	public List<Ueberhangmandat> getUeberhangmandate() {
		final List<Ueberhangmandat> resultList = em.createQuery(
				"SELECT s from Ueberhangmandat s", Ueberhangmandat.class)
				.getResultList();
		return resultList;
	}

	@Override
	public GesamteGewinnerVerlierer getKnappsteGewinnerVerlierer() {
		final List<Knappstesieger> gewinner = em.createQuery(
				"SELECT s from Knappstesieger s", Knappstesieger.class)
				.getResultList();
		final List<KnappsteVerlierer> verlierer = em.createQuery(
				"SELECT s from KnappsteVerlierer s", KnappsteVerlierer.class)
				.getResultList();
		GesamteGewinnerVerlierer result = new GesamteGewinnerVerlierer();
		result.setSieger(gewinner);
		result.setVerlierer(verlierer);
		return result;
	}

	@Override
	public List<Wahlkreisuebersicht> getWahlkreisuebersichtEinzelstimmen(
			int wahlkreisnr) {
		final TypedQuery<Wahlkreisuebersicht> q = em.createQuery(
				"SELECT s from Wahlkreisuebersicht s WHERE s.wahlkreisnr = ?1",
				Wahlkreisuebersicht.class);
		q.setParameter(1, wahlkreisnr);

		return q.getResultList();
	}

	@Override
	public List<WahlkreisuebersichtErststimme> getWahlkreisuebersichtErststimme(
			int wahlkreisnr) {
		final TypedQuery<WahlkreisuebersichtErststimme> q = em
				.createQuery(
						"SELECT e from WahlkreisuebersichtErststimme e WHERE e.wahlkreisnr = ?1",
						WahlkreisuebersichtErststimme.class);
		q.setParameter(1, wahlkreisnr);
		final List<WahlkreisuebersichtErststimme> resultList = q.getResultList();
		
		return resultList;
	}

	@Override
	public Void reset() {
		
		File buildFile = new File(this.getServletContext().getRealPath("/")+"build.xml");
		Project p = new Project();
		
		p.setUserProperty("ant.file", buildFile.getAbsolutePath());
		p.init();
		ProjectHelper helper = ProjectHelper.getProjectHelper();
		p.addReference("ant.projectHelper", helper);
		helper.parse(p, buildFile);
		p.executeTarget("run");
		return null;
	}
}
