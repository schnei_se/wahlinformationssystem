package de.tum.wahl.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    private static Database db = null;

    public static Database getInstance() {
	if (db == null)
	    db = new Database();
	return db;
    }

    final String host = "localhost";
    final String database = "wahl";
    final String user = "wahl";
    final String password = "wahl";

    final Connection connection;

    public Database() {
	try {
	    connection = DriverManager.getConnection(getUrl(), user, password);
	} catch (SQLException e) {
	    throw new RuntimeException(e);
	}
    }

    public Connection getConnection() {
	return connection;
    }

    /**
     * @return Url-string for postgreSQL-database connection
     */
    private String getUrl() {
	return ("jdbc:postgresql:" + (host != null ? ("//" + host) + "/" : "") + database);
    }

}
