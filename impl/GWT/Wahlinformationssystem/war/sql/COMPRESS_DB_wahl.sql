BEGIN WORK;
-- temporaere tabelle fuer erststimmen anlegen
DROP TABLE IF EXISTS Erststimmen_tmp;
CREATE TABLE Erststimmen_tmp (
	KandidatNr INT NOT NULL,
	WahlkreisNr INT NOT NULL,
	Jahr INT NOT NULL,
	anzahl int not null
);
-- sicherstellen, dass keine neuen erststimmen eingefuegt werden und FK-integritaet gewahrt bleibt.
LOCK TABLE Erststimmen, Kandidaten, Wahlkreise IN EXCLUSIVE MODE;
INSERT INTO Erststimmen_tmp (SELECT * FROM Erststimmen);
-- sicherstellen, dass nun auch nicht mehr aus erststimmen gelesen wird 
LOCK TABLE Erststimmen IN ACCESS EXCLUSIVE MODE;
DELETE FROM Erststimmen;
-- aggregation durchfuehren
INSERT INTO Erststimmen (SELECT KandidatNr, WahlkreisNr, Jahr, SUM(anzahl) FROM Erststimmen_tmp GROUP BY KandidatNr, WahlkreisNr, Jahr);
DROP TABLE Erststimmen_tmp;
COMMIT WORK;

BEGIN WORK;
DROP TABLE IF EXISTS Zweitstimmen_tmp;
CREATE TABLE Zweitstimmen_tmp (
	LandeslisteNr INT NOT NULL,
	WahlkreisNr INT NOT NULL,
	Jahr INT NOT NULL,
	anzahl int not null
);
LOCK TABLE Zweitstimmen, Landeslisten, Wahlkreise IN EXCLUSIVE MODE;
INSERT INTO Zweitstimmen_tmp (SELECT * FROM Zweitstimmen);
LOCK TABLE Zweitstimmen IN ACCESS EXCLUSIVE MODE;
DELETE FROM Zweitstimmen;
INSERT INTO Zweitstimmen (SELECT LandeslisteNr, WahlkreisNr, Jahr, SUM(anzahl) FROM Zweitstimmen_tmp GROUP BY LandeslisteNr, WahlkreisNr, Jahr);
DROP TABLE Zweitstimmen_tmp;
COMMIT WORK;