

DROP VIEW IF EXISTS Direktkandidaten0509 CASCADE;
CREATE VIEW Direktkandidaten0509(nr, wahlkreisNr, vorname, name, parteikuerzel, jahr, wahlbeteiligung) AS 
--EXPLAIN
  -- Zweitstimmen pro Wahlkreis 
  WITH AnzahlZweitstimmenProWahlkreis(wahlkreisnr, jahr, anzahl) AS (
    SELECT z.wahlkreisnr, z.jahr, SUM(z.anzahl) 
    FROM  Landeslisten l, Zweitstimmen z 
    WHERE l.nr = z.landeslistenr 
    GROUP BY z.wahlkreisnr, z.jahr
  ),
  
  -- Anzahl Wahlberechtigte pro Wahlkreis
  WahlbeteiligungProWahlkreis(wahlkreisnr, jahr, prozent) AS (
  	SELECT w.nr, a.jahr, 100.*(a.anzahl+w.AnzUngueltig2)/w.AnzWahlberechtigte
  	FROM Wahlkreise w, AnzahlZweitstimmenProWahlkreis a
  	WHERE w.nr = a.wahlkreisnr
   	
  ),
   
  -- Anzahl Erststimmen pro Kandidat und Wahlkreis
  ErststimmenProKandidat(kandidatNr, wahlkreisNr, jahr,  anzStimmen) AS (
    SELECT kandidatNr, wahlkreisNr, jahr, sum(anzahl)
    FROM Erststimmen
    GROUP BY kandidatNr, wahlkreisNr, jahr
  ),
  AnzahlMaxErststimmenProWahlkreis(wahlkreisNr, jahr, anzahl) AS (
    SELECT wahlkreisNr, jahr, MAX(anzStimmen) 
    FROM ErststimmenProKandidat
    GROUP BY wahlkreisNr, jahr
  ),
  -- Direktmandate pro Wahlkreis
  Direktmandate(wahlkreisNr, jahr, kandidatNr) AS (
    SELECT e.wahlkreisNr, e.jahr, e.kandidatNr
    FROM ErststimmenProKandidat e, AnzahlMaxErststimmenProWahlkreis f
    WHERE e.anzStimmen = f.anzahl
      AND e.wahlkreisNr = f.wahlkreisNr
      AND e.jahr = f.jahr
  )
  SELECT  k.nr, d.wahlkreisnr, k.vorname, k.name, k.parteikuerzel, d.jahr, w.prozent
  FROM Direktmandate d, Kandidaten k, WahlbeteiligungProWahlkreis w
  WHERE d.kandidatnr = k.nr AND w.wahlkreisnr = d.wahlkreisnr AND w.jahr = d.jahr;
  

  

  DROP VIEW IF EXISTS Parteien0509 CASCADE;
CREATE VIEW Parteien0509(nr, wahlkreisNr, jahr, parteikuerzel, relativ,absolut) AS 
--EXPLAIN
  -- Zweitstimmen pro Bundesland und Partei 
  WITH AnzahlZweitstimmenProParteiUWahlkreis(parteikuerzel, wahlkreisnr, jahr, anzahl) AS (
    SELECT l.parteikuerzel, z.wahlkreisnr, z.jahr, SUM(z.anzahl) 
    FROM  Landeslisten l, Zweitstimmen z 
    WHERE l.nr = z.landeslistenr 
    GROUP BY l.parteikuerzel, z.wahlkreisnr, z.jahr
  ),
  AnzahlZweitstimmenProWahlkreis(wahlkreisnr, jahr, anzahl) AS (
    SELECT z.wahlkreisnr, z.jahr, SUM(z.anzahl) 
    FROM  Landeslisten l, Zweitstimmen z 
    WHERE l.nr = z.landeslistenr 
    GROUP BY z.wahlkreisnr, z.jahr
  ),
  

  -- Prozentuale Anzahl der Stimmen für eine Partei pro Wahlkreis
  ZweitstimmenRelativProWahlkreis(parteiKuerzel, wahlkreisnr, jahr, quote) AS (
    SELECT a.parteiKuerzel, a.wahlkreisnr, a.jahr, 100.0*a.anzahl / az.anzahl 
    FROM AnzahlZweitstimmenProParteiUWahlkreis a, AnzahlZweitstimmenProWahlkreis az
    WHERE a.wahlkreisnr = az.wahlkreisnr AND a.jahr = az.jahr
  )
  -- vergleich 2005 09
  
   SELECT  round(random()*1000000),zr.wahlkreisnr, zr.jahr, zr.parteikuerzel, zr.quote, za.anzahl
  FROM ZweitstimmenRelativProWahlkreis zr, AnzahlZweitstimmenProParteiUWahlkreis za
  WHERE zr.wahlkreisnr = za.wahlkreisnr AND zr.jahr = za.jahr AND zr.parteikuerzel = za.parteikuerzel

 
  