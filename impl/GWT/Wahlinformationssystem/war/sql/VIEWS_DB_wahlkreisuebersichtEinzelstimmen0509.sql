DROP VIEW IF EXISTS WahlkreisuebersichtErststimmen0509 CASCADE;
CREATE VIEW WahlkreisuebersichtErststimmen0509(nr, wahlkreisNr, parteikuerzel,jahr,  relativ,absolut) AS 
--EXPLAIN
  -- Anzahl Erststimmen pro Kandidat und Wahlkreis
 	WITH ErststimmenProPartei(wahlkreisNr, parteikuerzel, jahr,  anzahl) AS (
    SELECT e.wahlkreisNr, k.parteikuerzel, e.jahr, sum(e.anzahl)
    FROM Erststimmen e, Kandidaten k
    WHERE e.kandidatnr = k.nr
    GROUP BY e.wahlkreisnr, k.parteikuerzel, e.jahr
  ),
  AnzahlErststimmenProWahlkreis(wahlkreisnr, jahr, anzahl) AS (
    SELECT e.wahlkreisnr, e.jahr, SUM(e.anzahl) 
    FROM  Erststimmen e 
    GROUP BY e.wahlkreisnr, e.jahr
  ),
  ErststimmenRelativProPartei(wahlkreisnr, parteikuerzel, jahr, relativ) AS (
  	SELECT epp.wahlkreisnr, epp.parteikuerzel, epp.jahr, 100.*epp.anzahl/ae.anzahl
  	FROM ErststimmenProPartei epp, AnzahlErststimmenProWahlkreis ae
  	WHERE epp.wahlkreisnr = ae.wahlkreisnr AND ae.jahr = epp.jahr
  )

  -- vergleich 2005 09
 
   SELECT  round(random()*1000000),erpp.wahlkreisnr,  erpp.parteikuerzel, erpp.jahr,erpp.relativ, epp.anzahl
  FROM ErststimmenRelativProPartei erpp, ErststimmenProPartei epp
  WHERE erpp.wahlkreisnr = epp.wahlkreisnr AND erpp.jahr = epp.jahr AND erpp.parteikuerzel = epp.parteikuerzel
  
  